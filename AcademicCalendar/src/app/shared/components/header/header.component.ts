import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/academicCalendarClient/services/authentication.service';

@Component({
    selector: 'app-shared-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class SharedHeaderComponent {

    isLoading = false;

    constructor(private authenticationService: AuthenticationService, private router: Router) {}

    isAuthenticated(): boolean {
        return this.authenticationService.isAuthenticated;
    }

    logout(): void {
        this.isLoading = true;
        this.authenticationService.logout().subscribe(
            response => {
                this.isLoading = false;
                this.router.navigateByUrl('/account/login');
            }
        );
    }

}
