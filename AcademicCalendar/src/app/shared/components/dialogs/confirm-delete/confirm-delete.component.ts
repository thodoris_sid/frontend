import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-user-courses-view-confirm-delete',
    templateUrl: './confirm-delete.component.html'
})
export class ConfirmDeleteComponent {

    deletedItem: string;
    type: string;

    constructor(private dialogRef: MatDialogRef<ConfirmDeleteComponent>, @Inject(MAT_DIALOG_DATA) private data: any) {
        this.deletedItem = data.deletedItem;
        this.type = data.type;
    }

    close(): void {
        this.dialogRef.close();
    }

    delete(): void {
        this.dialogRef.close({action: 'delete'});
    }

}
