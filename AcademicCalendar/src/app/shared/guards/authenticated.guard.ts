import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { AuthenticationService } from 'src/app/academicCalendarClient/services/authentication.service';

@Injectable()
export class AuthenticatedGuard implements CanActivate {
    constructor(private authenticationService: AuthenticationService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree {
        if (this.authenticationService.isAuthenticated) {
            return true;
        }

        const url = `/account/login?next=${state.url}`;
        return this.router.parseUrl(url);
    }
}
