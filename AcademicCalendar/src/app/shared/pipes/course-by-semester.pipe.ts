import { Pipe, PipeTransform } from '@angular/core';
import { groupBy } from 'src/app/globals';

@Pipe({
    name: 'courseBySemester'
})

export class CourseBySemesterPipe implements PipeTransform {

    transform(value: any[], semester: number): any {
        const grouped = groupBy(value, event => event.semester);
        return grouped.get(semester) ? grouped.get(semester) : [];
    }

}
