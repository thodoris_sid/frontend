import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
    name: 'eventTime'
})
export class FormatEventTimePipe implements PipeTransform {
    transform(event: Date | undefined): string | null {
        return moment.utc(event).local().format('HH:mm');
    }
}
