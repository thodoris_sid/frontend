import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
    name: 'eventDate'
})
export class FormatEventDatePipe implements PipeTransform {
    transform(event: Date | undefined): string | null {
        return moment.utc(event).local().locale('el').format('dddd D MMMM YYYY');
    }
}
