import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';

import { SharedHeaderComponent, SnackBarComponent } from './components';
import { AuthenticatedGuard } from './guards';
import { CourseBySemesterPipe, FormatEventDatePipe, FormatEventTimePipe, FormatEventDateTimePipe } from './pipes';
import { ConfirmDeleteComponent } from './components/dialogs/confirm-delete/confirm-delete.component';

@NgModule({
    imports: [
        CommonModule, RouterModule, MatButtonModule, MatSnackBarModule, MatToolbarModule
    ],
    providers: [
        AuthenticatedGuard
    ],
    declarations: [SharedHeaderComponent, SnackBarComponent, FormatEventDatePipe, FormatEventTimePipe, FormatEventDateTimePipe,
                   CourseBySemesterPipe, ConfirmDeleteComponent],
    exports: [SharedHeaderComponent, FormatEventDatePipe, FormatEventTimePipe, FormatEventDateTimePipe,
              CourseBySemesterPipe, ConfirmDeleteComponent]
})
export class SharedModule { }
