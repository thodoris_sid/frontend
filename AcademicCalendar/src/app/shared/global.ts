import { FormControl } from '@angular/forms';

export function getStudiesType(studiesType: string | null): number {
    if (studiesType === 'PPS') {
        return 1;
    } else if (studiesType === 'PMS1') {
        return 2;
    } else if (studiesType === 'PMS2') {
        return 3;
    } else {
        return 4;
    }
}

export function noWhitespace(control: FormControl): { whitespace: boolean; } | null {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
}
