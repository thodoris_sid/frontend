import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ProgramStudiesAPIService } from '../api/programStudies.api.service';
import { CacheService } from '../cache/cache.service';
import { ProgramStudies } from '../models/programStudies.model';

@Injectable()
export class ProgramStudiesService {

    constructor(private programStudiesAPIService: ProgramStudiesAPIService, private cacheService: CacheService) {}

    add(name: string, urlParameter: string): Observable<any> {
        return this.programStudiesAPIService.add(name, urlParameter).pipe(
            tap(_ => {
                this.cacheService.programStudiesAdded();
            })
        );
    }

    getAll(): Observable<Array<ProgramStudies>> | undefined {
        return this.cacheService.getAllProgramStudies();
    }

    delete(studiesId: number | undefined): Observable<any> {
        return this.programStudiesAPIService.delete(studiesId).pipe(
            tap(_ => {
                this.cacheService.programStudiesAdded();
            })
        );
    }

}
