import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ProfessorAPIService } from '../api/professor.api.service';
import { CacheService } from '../cache/cache.service';
import { AddProfessor, Professor } from '../models/professor.model';

@Injectable()
export class ProfessorService {

    constructor(private professorAPIService: ProfessorAPIService, private cacheService: CacheService) {}

    add(professor: AddProfessor): Observable<any> {
        return this.professorAPIService.add(professor).pipe(
            tap(_ => {
                this.cacheService.professorAdded();
            })
        );
    }

    update(professor: AddProfessor, professorId: number): Observable<any> {
        return this.professorAPIService.update(professor, professorId).pipe(
            tap(_ => {
                this.cacheService.professorAdded();
            })
        );
    }

    get(id: number): Observable<Professor> | undefined {
        return this.professorAPIService.get(id);
    }

    getAll(): Observable<Array<Professor>> | undefined {
        return this.cacheService.getAllProfessors();
    }

    delete(professorId: number | null): Observable<any> {
        return this.professorAPIService.delete(professorId).pipe(
            tap(_ => {
                this.cacheService.professorAdded();
            }));
    }

}
