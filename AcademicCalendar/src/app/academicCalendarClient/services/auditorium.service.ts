import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuditoriumAPIService } from '../api/auditorium.api.service';
import { CacheService } from '../cache/cache.service';
import { AddAuditorium, Auditorium } from '../models/auditorium.model';

@Injectable()
export class AuditoriumService {

    constructor(private auditoriumAPIService: AuditoriumAPIService, private cacheService: CacheService) {}

    add(auditorium: AddAuditorium): Observable<any> {
        return this.auditoriumAPIService.add(auditorium).pipe(
            tap(_ => {
                this.cacheService.auditoriumUpdated();
            })
        );
    }

    update(auditorium: AddAuditorium, auditoriumId: number): Observable<any> {
        return this.auditoriumAPIService.update(auditorium, auditoriumId).pipe(
            tap(_ => {
                this.cacheService.auditoriumUpdated();
            })
        );
    }

    get(id: number): Observable<Auditorium> | undefined {
        return this.auditoriumAPIService.get(id);
    }

    delete(auditoriumId: number | undefined): Observable<any> {
        return this.auditoriumAPIService.delete(auditoriumId).pipe(
            tap(_ => {
                this.cacheService.auditoriumUpdated();
            })
        );
    }

    getAll(): Observable<Array<Auditorium>> | undefined {
        return this.cacheService.getAllAuditoriums();
    }

}
