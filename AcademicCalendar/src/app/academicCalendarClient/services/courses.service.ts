import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { CoursesAPIService } from '../api/courses.api.service';
import { CacheService } from '../cache/cache.service';
import { AddCourse, Course, CourseWithProfessor } from '../models/course.model';

@Injectable()
export class CourseService {

    constructor(private courseAPIService: CoursesAPIService, private cacheService: CacheService) {}

    add(course: AddCourse): Observable<any> {
        return this.courseAPIService.add(course).pipe(
            tap(_ => {
                this.cacheService.courseAdded();
            })
        );
    }

    getAll(studiesType: number): Observable<Array<CourseWithProfessor>> | undefined {
        return this.cacheService.getAllCourses(studiesType);
    }

    getAllFromSemester(semester: string, studiesType: number): Observable<Array<CourseWithProfessor>> | undefined {
        return this.cacheService.getAllCourses(studiesType)?.pipe(
            map(
                response => response.filter(course => course.Semester === +semester)
            )
        );
    }

    get(courseId: string | null | undefined): Observable<CourseWithProfessor> {
        return this.courseAPIService.get(courseId);
    }

    delete(courseId: string | null): Observable<any> {
        return this.courseAPIService.delete(courseId).pipe(
            tap(_ => {
                this.cacheService.courseAdded();
            })
        );
    }

}
