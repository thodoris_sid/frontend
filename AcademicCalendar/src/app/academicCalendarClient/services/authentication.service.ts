import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticationAPIService } from '../api/authentication.api.service';
import { SuccessResponse } from '../models/general.model';
import { NewAdmin } from '../models/oAuth.model';
import { OAuthService } from '../oAuth/oAuth.service';

@Injectable()
export class AuthenticationService {
    constructor(private oAuthService: OAuthService, private authenticationAPIService: AuthenticationAPIService) {
    }

    register(admin: NewAdmin): Observable<any> {
        return this.authenticationAPIService.register(admin);
    }

    tryLogin(username: string, password: string, rememberMe: boolean): Observable<boolean> {
        return this.oAuthService.tryCredentialsLogin(username, password, rememberMe);
    }

    logout(): Observable<SuccessResponse> {
        return this.oAuthService.logout();
    }

    get isAuthenticated(): boolean {
        return this.oAuthService.isAuthenticated;
    }
}
