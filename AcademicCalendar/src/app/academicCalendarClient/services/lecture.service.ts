import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { LectureAPIService } from '../api/lectures.api.service';
import { CacheService } from '../cache/cache.service';
import { Lecture } from '../models/lecture.model';

@Injectable()
export class LectureService {

    constructor(private lectureAPIService: LectureAPIService, private cacheService: CacheService) {}

    add(course: any): Observable<any> {
        return this.lectureAPIService.add(course).pipe(
            tap(_ => {
                this.cacheService.lectureDeleted();
            })
        );
    }

    delete(id: number): Observable<any> {
        return this.lectureAPIService.delete(id).pipe(
            tap(_ => {
                this.cacheService.lectureDeleted();
            })
        );
    }

    deleteAll(): Observable<any> {
        return this.lectureAPIService.deleteAll().pipe(
            tap(_ => {
                this.cacheService.lectureDeleted();
            })
        );
    }

    getAll(studiesType: number, semester: number | undefined): Observable<Array<Lecture>> | undefined {
        return this.cacheService.getAllLectures(studiesType)?.pipe(
            map(
                response => response.filter(lecture => semester && semester !== 0 ? lecture.Semester === +semester : true)
            )
        );
    }

    getLecturesFromAllProgramStudies(studiesType: number, semester: number | undefined): Observable<Array<Lecture>> | undefined {
        return this.cacheService.getLecturesFromAllProgramStudies()?.pipe(
            map(
                response => response.filter(lecture => semester && semester !== 0 ?
                    lecture.Semester === +semester : true)
            )
        );
    }
}
