import { Injectable } from '@angular/core';

@Injectable()
export class FiltersService {

    constructor() {
    }

    removeAcc(text: string): string {

        text = text.replace(/ά/g, 'α');
        text = text.replace(/έ/g, 'ε');
        text = text.replace(/ή/g, 'η');
        text = text.replace(/ί|ΐ|ϊ/g, 'ι');
        text = text.replace(/ό/g, 'ο');
        text = text.replace(/ύ|ΰ|ϋ/g, 'υ');
        text = text.replace(/ώ/g, 'ω');

        return text;

    }

    trim(text: string): string {

        text = text.trim();

        return text;
    }

    removeAccents(text: string): string {
        if (text) {
            text = this.removeAcc(text);
            text = this.trim(text);
        }

        return text;
    }

}
