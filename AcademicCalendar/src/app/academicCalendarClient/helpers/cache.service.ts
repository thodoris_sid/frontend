import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';

@Injectable()
export class CacheHelperService {
    private cache: { [key: string]: ReplaySubject<any> | undefined } = {};

    get<T>(key: string, setter: Observable<T>, expirationInMilis: number = 0): ReplaySubject<T> | undefined {
        if (!this.cache[key]) {
            this.cache[key] = new ReplaySubject<T>(1);

            setter.subscribe(response => {
                this.cache[key]?.next(response);
            });

            setTimeout(() => { this.remove(key); }, expirationInMilis);
        }

        return this.cache[key];
    }

    remove(key: string): void {
        for (const cacheKey in this.cache) {
            if (cacheKey.match(key)) {
                this.cache[cacheKey] = undefined;
            }
        }
    }

    removeAll(): void {
        this.cache = {};
    }
}
