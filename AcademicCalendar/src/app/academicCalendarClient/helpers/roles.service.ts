import { Injectable } from '@angular/core';
import * as Cookies from 'js-cookie';
import { Configuration } from '../models/configuration.model';
import { OAuthStoragedToken } from '../models/oAuth.model';

@Injectable()
export class RolesService {
    constructor(private configuration: Configuration) {
    }

    setRole(): void {
        const token = Cookies.get(this.configuration.oAuthStorageName) ?? '';
        const stringifyToken = token ? JSON.parse(token) : '';
        if (stringifyToken && stringifyToken.roles.toLowerCase().indexOf('user') > 0) {
          sessionStorage.setItem('role', 'user');
        } else if (stringifyToken && stringifyToken.roles.toLowerCase().indexOf('superadmin') > 0) {
          sessionStorage.setItem('role', 'superadmin');
        } else if (stringifyToken && stringifyToken.roles.toLowerCase().indexOf('admin') > 0) {
          sessionStorage.setItem('role', 'admin');
        }
    }

    isAdmin(): boolean {
      if (sessionStorage.getItem('role') === 'admin' || sessionStorage.getItem('role') === 'superadmin' ){
        return true;
      } else {
        return false;
      }
    }

    isSuperAdmin(): boolean {
      if (sessionStorage.getItem('role') === 'superadmin') {
        return true;
      } else {
        return false;
      }
    }

}

