import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class StudiesService {

    currentStudies = new BehaviorSubject<string>('PPS');

    changeCurrent(state: string): void {
        this.currentStudies.next(state);
    }

}
