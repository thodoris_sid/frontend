import { Injectable } from '@angular/core';

import { Configuration } from '../models/configuration.model';

@Injectable()
export class UrlService {
    constructor(private configuration: Configuration) { }

    urlFor(path: string[]): string {
        let url = this.getBaseUrl();

        url += ('/' + path.join('/'));

        return url;
    }

    queryStringToJSON(query: string, delimiter: string = '&'): { [key: string]: string } {
        const parts: { [key: string]: string } = {};

        decodeURIComponent(query.slice(1)).split(delimiter).forEach((part) => {
            const pair = part.split('=');
            parts[pair[0]] = decodeURIComponent(pair[1] || '');
        });

        return parts;
    }

    private getBaseUrl(): string {
        return this.configuration.baseUrl;
    }
}
