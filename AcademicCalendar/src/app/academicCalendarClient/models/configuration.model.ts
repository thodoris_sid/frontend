export class Configuration {
    baseUrl: string;

    oAuthStorageExpirationInDays: number;
    oAuthStorageName: string;
    oAuthStorageSecured: boolean;
}
