export interface Lecture {
    LectureId: number;
    StartTime: string;
    EndTime: string;
    CourseId: number;
    StudiesId: number;
    CourseName: string;
    Semester: number;
    AuditoriumName: string;
    AuditoriumId: number;
    auditoriumId: number;
    ProfessorId: number;
    professorId: number;
    ProfessorFirstName: string;
    professorFirstName: string;
    ProfessorLastName: string;
    professorLastName: string;
    start: Date;
    end: Date;
}
