import { AddEquipment, Equipment } from './equipment.model';

export interface Auditorium {
    AuditoriumId: number | undefined;
    Name: string;
    Capacity: number;
    Type: AuditoriumType;
    Equipment: Array<Equipment>;
}

export enum AuditoriumType {
    classrooms = 0,
    laboratories = 10
}

export interface AddAuditorium {
    Name: string;
    Capacity: number;
    Type: number;
    Equipment: Array<AddEquipment>;
}
