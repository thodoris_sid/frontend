import { AddEquipment, Equipment } from './equipment.model';
import { Professor } from './professor.model';

export interface Course {
    CourseId: number;
    CourseCode: string;
    Description: string;
    Title: string;
    Semester: number;
    IsInCalendar: boolean;
    IsObligatory: boolean;
}

export interface CourseWithProfessor {
    CourseId: number;
    CourseCode: string;
    Description: string;
    Title: string;
    Semester: number;
    Professor: Professor;
    IsInCalendar: boolean;
    IsObligatory: boolean;
    Equipment: Array<Equipment>;
}

export interface AddCourse {
    CourseCode: string;
    Semester: number;
    Title: string;
    Description: string;
    ProfessorId: number;
    StudiesId: number;
    IsObligatory: boolean;
    Equipment: Array<AddEquipment>;
}
