export interface Professor {
    ProfessorId: number;
    FirstName: string;
    LastName: string;
    Description: string;
    Office: string;
    Phone: string;
    Website: string;
    Email: string;
    Type: ProfessorType;
}

export enum ProfessorType {
    'dep' = 0,
    'edip' = 10,
    'exwterikoi' = 20
}

export interface AddProfessor {
    Firstname: string;
    Lastname: string;
    Description: string;
    Office: string;
    Phone: string;
    Email: string;
    Type: ProfessorType;
    Website: string;
}
