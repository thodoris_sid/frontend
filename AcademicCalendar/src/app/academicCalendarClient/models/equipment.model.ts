export interface Equipment {
    EquipmentId: number;
    Type: string;
    TypeCount: number;
}

export interface AddEquipment {
    Type: string;
    TypeCount: number;
}
