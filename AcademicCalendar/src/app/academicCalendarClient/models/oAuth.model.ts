export interface OAuthToken {
    accessToken: string;
    refreshToken: string;
    expiresAt: Date;
    roles: Array<string>;
}

export interface OAuthStoragedToken extends OAuthToken {
    sessionExpiration: boolean;
}

export interface NewAdmin {
    Email: string;
    Password: string;
    ConfirmPassword: string;
}
