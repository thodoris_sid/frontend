export interface ProgramStudies {
    StudiesId: number;
    Name: string;
    UrlParameter: string;
}

export interface AddProgramStudies {
    Name: string;
    UrlParameter: string;
}

