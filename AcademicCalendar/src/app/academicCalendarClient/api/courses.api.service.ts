import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UrlService } from '../helpers/url.service';
import { AddCourse, Course, CourseWithProfessor } from '../models/course.model';

@Injectable()
export class CoursesAPIService {

    constructor(private http: HttpClient, private urlService: UrlService) { }

    add(course: AddCourse): Observable<any> {
        const url: string = this.urlService.urlFor(['api', 'courses', 'add']);

        return this.http.post<any>(url, course);
    }

    getAll(studiesType: number): Observable<Array<CourseWithProfessor>> {
        const url: string = this.urlService.urlFor(['api', 'courses', 'studiesType', studiesType.toString()]);

        return this.http.get<Array<CourseWithProfessor>>(url);
    }

    getAllFromSemester(semester: string | null | undefined): Observable<Array<Course>> {
        const url: string = this.urlService.urlFor(['api', 'courses', 'semester', semester === null
        || semester === undefined ? '' : semester]);

        return this.http.get<Array<Course>>(url);
    }

    get(courseId: string | null | undefined): Observable<CourseWithProfessor> {
        const url: string = this.urlService.urlFor(['api', 'courses', courseId === null
        || courseId === undefined ? '' : courseId]);

        return this.http.get<CourseWithProfessor>(url);
    }

    delete(courseId: string | null): Observable<any> {
        const url: string = this.urlService.urlFor(['api', 'courses', 'delete', courseId === null
        ? '' : courseId]);

        return this.http.post<any>(url, {});
    }

}
