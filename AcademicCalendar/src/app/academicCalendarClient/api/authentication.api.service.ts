import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UrlService } from '../helpers/url.service';
import { SuccessResponse } from '../models/general.model';
import { NewAdmin, OAuthToken } from '../models/oAuth.model';

@Injectable()
export class AuthenticationAPIService {
    constructor(private urlService: UrlService, private http: HttpClient) {
    }

    register(admin: NewAdmin): Observable<any> {
        const url: string = this.urlService.urlFor(['api', 'Account', 'Register']);

        return this.http.post<any>(url, admin);
    }

    getToken(username: string, password: string): Observable<OAuthToken> {
        const params: HttpParams = new HttpParams({
            fromObject: {
                grant_type: 'password',
                username,
                password
            }
        });

        return this.requestForToken(params);
    }

    refreshToken(token: OAuthToken | undefined): Observable<OAuthToken> {
        const params: HttpParams = new HttpParams({
            fromObject: {
                grant_type: 'refresh_token',
                refresh_token: (token ? token.refreshToken : '')
            }
        });

        return this.requestForToken(params);
    }


    logout(): Observable<SuccessResponse> {
        const url: string = this.urlService.urlFor(['api', 'Account', 'Logout']);

        return this.http.post<SuccessResponse>(url, {});
    }

    private requestForToken(params: HttpParams): Observable<OAuthToken> {
        const url: string = this.urlService.urlFor(['Token']);

        const headers: HttpHeaders = new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded'
        });

        return this.http.post(url, params.toString(), {
            headers
        }).pipe(
            map(response => this.toOauthTokenModel(response))
        );
    }

    private toOauthTokenModel(response: any): OAuthToken {
        return {
            accessToken : response.access_token,
            refreshToken : response.refresh_token,
            expiresAt: new Date(response['.expires']),
            roles: response.roles
        };
    }
}
