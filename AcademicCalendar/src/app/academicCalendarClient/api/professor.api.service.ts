import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UrlService } from '../helpers/url.service';
import { AddProfessor, Professor } from '../models/professor.model';

@Injectable()
export class ProfessorAPIService {

    constructor(private http: HttpClient, private urlService: UrlService) { }

    add(professor: AddProfessor): Observable<any> {
        const url: string = this.urlService.urlFor(['api', 'professors', 'add']);

        return this.http.post<any>(url, professor);
    }

    update(professor: AddProfessor, professorId: number): Observable<any> {
        const url: string = this.urlService.urlFor(['api', 'professors', professorId.toString(), 'update']);

        return this.http.post<any>(url, professor);
    }

    get(id: number): Observable<Professor> {
        const url: string = this.urlService.urlFor(['api', 'professors', id.toString()]);

        return this.http.get<Professor>(url);
    }

    delete(professorId: number | null): Observable<any> {
        const url: string = this.urlService.urlFor(['api', 'professors', 'delete', professorId === null
        ? '' : professorId.toString()]);

        return this.http.post<any>(url, {});
    }

    getAll(): Observable<Array<Professor>> {
        const url: string = this.urlService.urlFor(['api', 'professors']);

        return this.http.get<Array<Professor>>(url);
    }
}
