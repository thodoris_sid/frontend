import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UrlService } from '../helpers';
import { ProgramStudies } from '../models/programStudies.model';

@Injectable()
export class ProgramStudiesAPIService {

    constructor(private http: HttpClient, private urlService: UrlService) { }

    add(name: string, urlParameter: string): Observable<any> {
        const url: string = this.urlService.urlFor(['api', 'programStudies', 'add']);

        return this.http.post<any>(url, {name, urlParameter});
    }

    getAll(): Observable<Array<ProgramStudies>> {
        const url: string = this.urlService.urlFor(['api', 'programStudies']);

        return this.http.get<Array<ProgramStudies>>(url);
    }

    delete(studiesId: number | undefined): Observable<any> {
        const url: string = this.urlService.urlFor(['api', 'programStudies', 'delete', studiesId === undefined
        ? '' : studiesId.toString()]);

        return this.http.post<any>(url, {});
    }

}
