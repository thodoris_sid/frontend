import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UrlService } from '../helpers';
import { Lecture } from '../models/lecture.model';

@Injectable()
export class LectureAPIService {

    constructor(private http: HttpClient, private urlService: UrlService) { }

    add(course: any): Observable<any> {
        const url: string = this.urlService.urlFor(['api', 'lectures', 'add']);
        return this.http.post<any>(url, course);
    }

    delete(id: number): Observable<any> {
        const url: string = this.urlService.urlFor(['api', 'lectures', 'delete', id.toString()]);

        return this.http.post<any>(url, {});
    }

    deleteAll(): Observable<any> {
        const url: string = this.urlService.urlFor(['api', 'lectures', 'delete']);

        return this.http.post<any>(url, {});
    }

    getAll(studiesType: number): Observable<Array<Lecture>> {
        const url: string = this.urlService.urlFor(['api', 'lectures', 'studiesType', studiesType.toString()]);

        return this.http.get<Array<Lecture>>(url);
    }

    getLecturesFromAllProgramStudies(): Observable<Array<Lecture>> {
        const url: string = this.urlService.urlFor(['api', 'lectures']);

        return this.http.get<Array<Lecture>>(url);
    }

}
