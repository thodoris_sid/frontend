import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UrlService } from '../helpers/url.service';
import { AddAuditorium, Auditorium } from '../models/auditorium.model';

@Injectable()
export class AuditoriumAPIService {

    constructor(private http: HttpClient, private urlService: UrlService) { }

    add(auditorium: AddAuditorium): Observable<any> {
        const url: string = this.urlService.urlFor(['api', 'auditoriums', 'add']);

        return this.http.post<any>(url, auditorium);
    }

    update(auditorium: AddAuditorium, auditoriumId: number): Observable<any> {
        const url: string = this.urlService.urlFor(['api', 'auditoriums', auditoriumId.toString(), 'update']);

        return this.http.post<any>(url, auditorium);
    }

    get(id: number): Observable<Auditorium> {
        const url: string = this.urlService.urlFor(['api', 'auditoriums', id.toString()]);

        return this.http.get<Auditorium>(url);
    }

    delete(auditoriumId: number | undefined): Observable<any> {
        const url: string = this.urlService.urlFor(['api', 'auditoriums', 'delete', auditoriumId === undefined
        ? '' : auditoriumId.toString()]);

        return this.http.post<any>(url, {});
    }

    getAll(): Observable<Array<Auditorium>> {
        const url: string = this.urlService.urlFor(['api', 'auditoriums']);

        return this.http.get<Array<Auditorium>>(url);
    }
}
