import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { AuthenticationAPIService } from '../api/authentication.api.service';
import { CacheHelperService } from '../helpers';
import { SuccessResponse } from '../models/general.model';
import { OAuthStorageService } from './oAuthStorage.service';


@Injectable()
export class OAuthService {
    constructor(private oAuthStorageService: OAuthStorageService, private authenticationAPIService: AuthenticationAPIService,
                private cacheHelperService: CacheHelperService) {
    }

    tryCredentialsLogin(username: string, password: string, rememberMe: boolean): Observable<boolean> {
        return this.authenticationAPIService.getToken(username, password).pipe(
            map(
                token => {
                    this.oAuthStorageService.save(token, !rememberMe);
                    return true;
                }
            ),
            catchError (
                _ => {
                    return of(false);
                }
            )
        );
    }

    logout(): Observable<SuccessResponse> {
        return this.authenticationAPIService.logout().pipe(
            tap(_ => { this.cacheHelperService.removeAll(); this.oAuthStorageService.delete(); sessionStorage.removeItem('role');
                       sessionStorage.removeItem('studies'); })
        );
    }

    get isAuthenticated(): boolean {
        if (this.oAuthStorageService.token) {
            return true;
        }
        return false;
    }
}
