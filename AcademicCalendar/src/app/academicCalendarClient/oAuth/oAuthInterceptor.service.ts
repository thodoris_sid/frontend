import { HttpErrorResponse, HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap, switchMap } from 'rxjs/operators';
import { AuthenticationAPIService } from '../api/authentication.api.service';
import { Configuration } from '../models/configuration.model';
import { OAuthToken } from '../models/oAuth.model';
import { OAuthStorageService } from './oAuthStorage.service';

@Injectable()
export class OAuthInterceptorService {
    constructor(private oAuthStorageService: OAuthStorageService, private configuration: Configuration,
                private authenticationAPIService: AuthenticationAPIService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const authorizedRequest = this.authorizeRequest(request);

        return next.handle(authorizedRequest).pipe(
            catchError(
                error => {
                    if (error instanceof HttpErrorResponse && error.status === 401) {
                        return this.refreshToken().pipe(
                            switchMap(() => next.handle(this.authorizeRequest(request)))
                        );
                    }
                    return throwError(error);
                }
            )
        );
    }

    private refreshToken(): Observable<OAuthToken> {
        return this.authenticationAPIService.refreshToken(this.oAuthStorageService.token).pipe(
            catchError(
                errorResponse => {
                    this.oAuthStorageService.delete();
                    location.reload();

                    return throwError(errorResponse);
                }
            ),
            tap(
                token => {
                    this.oAuthStorageService.update(token);
                }
            )
        );
    }

    private authorizeRequest(request: HttpRequest<any>): HttpRequest<any> {
        if (request.url.indexOf(this.configuration.baseUrl) < 0) {
            return request;
        }

        if (request.url.indexOf('/Token') >= 0) {
            return request;
        }

        return request.clone({
            setHeaders: {
                ['Authorization']: `Bearer ${this.oAuthStorageService.accessToken}`
            }
        });
    }
}
