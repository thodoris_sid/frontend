import { Injectable } from '@angular/core';
import * as Cookies from 'js-cookie';

import { Configuration } from '../models/configuration.model';
import { OAuthToken, OAuthStoragedToken } from '../models/oAuth.model';

@Injectable()
export class OAuthStorageService {
    constructor(private configuration: Configuration) {
    }

    save(token: OAuthToken, sessionCookie: boolean): void {
        const cookie: OAuthStoragedToken = {
            ...token,
            sessionExpiration : sessionCookie
        };

        let expiration: any = null;
        if (!sessionCookie) {
            expiration = new Date();
            expiration.setDate(expiration.getDate() + this.configuration.oAuthStorageExpirationInDays);
        }

        Cookies.set(this.configuration.oAuthStorageName, JSON.stringify(cookie), {
            expires: expiration,
            path: '/',
            secure: this.configuration.oAuthStorageSecured
        });
    }

    update(token: OAuthToken): void {
        this.save(token, this.sessionExpiration);
    }

    delete(): void {
        Cookies.remove(this.configuration.oAuthStorageName, {
            path: '/'
        });
    }

    get token(): OAuthToken | undefined {
        return this.getToken();
    }

    get accessToken(): string {
        const token = this.getToken();
        if (!token) {
            return '';
        }
        return token.accessToken;
    }

    get refreshToken(): string {
        const token = this.getToken();
        if (!token) {
            return '';
        }
        return token.refreshToken;
    }

    get sessionExpiration(): boolean {
        const storedToken = this.getStoragedToken();
        if (!storedToken) {
            return false;
        }
        return storedToken.sessionExpiration;
    }

    private getToken(): OAuthToken | undefined {
        const cookie = Cookies.get(this.configuration.oAuthStorageName);
        if (!cookie) {
            return undefined;
        }

        const parsedCookie = JSON.parse(cookie);
        return {
            accessToken : parsedCookie.accessToken,
            refreshToken : parsedCookie.refreshToken,
            expiresAt: new Date(parsedCookie.expires),
            roles: parsedCookie.roles
        };
    }

    private getStoragedToken(): OAuthStoragedToken | undefined {
        const cookie = Cookies.get(this.configuration.oAuthStorageName);
        if (!cookie) {
            return undefined;
        }

        const parsedCookie = JSON.parse(cookie);
        return {
            accessToken : parsedCookie.accessToken,
            refreshToken : parsedCookie.refreshToken,
            expiresAt: new Date(parsedCookie.expires),
            sessionExpiration: parsedCookie.sessionExpiration,
            roles: parsedCookie.roles
        };
    }
}
