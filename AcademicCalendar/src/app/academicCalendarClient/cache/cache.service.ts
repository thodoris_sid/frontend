import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuditoriumAPIService } from '../api/auditorium.api.service';
import { CoursesAPIService } from '../api/courses.api.service';
import { LectureAPIService } from '../api/lectures.api.service';
import { ProfessorAPIService } from '../api/professor.api.service';
import { ProgramStudiesAPIService } from '../api/programStudies.api.service';
import { CacheHelperService } from '../helpers';
import { Auditorium } from '../models/auditorium.model';
import { CourseWithProfessor } from '../models/course.model';
import { Lecture } from '../models/lecture.model';
import { Professor } from '../models/professor.model';
import { ProgramStudies } from '../models/programStudies.model';

@Injectable()
export class CacheService {
    constructor(private cacheHelperService: CacheHelperService, private auditoriumAPIService: AuditoriumAPIService,
                private courseAPIService: CoursesAPIService, private lecturesAPIService: LectureAPIService,
                private professorAPIService: ProfessorAPIService, private programStudiesAPIService: ProgramStudiesAPIService) {
    }

    getAllAuditoriums(): Observable<Array<Auditorium>> | undefined {
        const cacheKey = 'getAllAuditoriums';

        const result = this.cacheHelperService.get<Array<Auditorium>>(cacheKey, this.auditoriumAPIService.getAll(), 600000);

        return result?.asObservable();
    }

    getAllCourses(studiesType: number): Observable<Array<CourseWithProfessor>> | undefined {
        const cacheKey = 'getAllCourses';

        const result = this.cacheHelperService.get<Array<CourseWithProfessor>>(cacheKey, this.courseAPIService.getAll(studiesType), 600000);

        return result?.asObservable();
    }

    getAllLectures(studiesType: number): Observable<Array<Lecture>> | undefined {
        const cacheKey = 'getAllLectures';

        const result = this.cacheHelperService.get<Array<Lecture>>(cacheKey, this.lecturesAPIService.getAll(studiesType), 600000);

        return result?.asObservable();
    }

    getLecturesFromAllProgramStudies(): Observable<Array<Lecture>> | undefined {
        const cacheKey = 'getLecturesFromAllProgramStudies';

        const result = this.cacheHelperService.get<Array<Lecture>>(cacheKey,
            this.lecturesAPIService.getLecturesFromAllProgramStudies(), 600000);

        return result?.asObservable();
    }

    getAllProfessors(): Observable<Array<Professor>> | undefined {
        const cacheKey = 'getAllProfessors';

        const result = this.cacheHelperService.get<Array<Professor>>(cacheKey, this.professorAPIService.getAll(), 600000);

        return result?.asObservable();
    }

    getAllProgramStudies(): Observable<Array<ProgramStudies>> | undefined {
        const cacheKey = 'getAllProgramStudies';

        const result = this.cacheHelperService.get<Array<ProgramStudies>>(cacheKey, this.programStudiesAPIService.getAll(), 600000);

        return result?.asObservable();
    }

    auditoriumUpdated(): void {
        this.cacheHelperService.remove('getAllAuditoriums');
    }

    courseAdded(): void {
        this.cacheHelperService.remove('getAllCourses');
    }

    lectureDeleted(): void {
        this.cacheHelperService.remove('getAllCourses');
    }

    professorAdded(): void {
        this.cacheHelperService.remove('getAllProfessors');
    }

    programStudiesAdded(): void {
        this.cacheHelperService.remove('getAllProgramStudies');
    }

}
