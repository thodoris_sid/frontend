import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { AuditoriumAPIService } from './api/auditorium.api.service';
import { AuthenticationAPIService } from './api/authentication.api.service';
import { CoursesAPIService } from './api/courses.api.service';
import { LectureAPIService } from './api/lectures.api.service';
import { ProfessorAPIService } from './api/professor.api.service';
import { ProgramStudiesAPIService } from './api/programStudies.api.service';
import { CacheService } from './cache/cache.service';
import { CacheHelperService, FiltersService, RolesService, StudiesService, UrlService } from './helpers';
import { Configuration } from './models/configuration.model';
import { OAuthService } from './oAuth/oAuth.service';
import { OAuthInterceptorService } from './oAuth/oAuthInterceptor.service';
import { OAuthStorageService } from './oAuth/oAuthStorage.service';
import { AuditoriumService } from './services/auditorium.service';
import { AuthenticationService } from './services/authentication.service';
import { CourseService } from './services/courses.service';
import { LectureService } from './services/lecture.service';
import { ProfessorService } from './services/professor.service';
import { ProgramStudiesService } from './services/programStudies.service';

@NgModule({
    imports: [
        HttpClientModule
    ],
    providers: [
        OAuthService, OAuthStorageService, OAuthInterceptorService, AuthenticationService,
        AuthenticationAPIService, UrlService, AuditoriumService, AuditoriumAPIService,
        CourseService, CoursesAPIService, ProfessorService, ProfessorAPIService, RolesService,
        LectureService, LectureAPIService, CacheHelperService, CacheService, StudiesService,
        FiltersService, ProgramStudiesService, ProgramStudiesAPIService,
        { provide: HTTP_INTERCEPTORS, useClass: OAuthInterceptorService, multi: true }
    ]
})
export class AcademicCalendarClientModule {
    static forRoot(configuration: Configuration): ModuleWithProviders<AcademicCalendarClientModule> {
        return {
          ngModule: AcademicCalendarClientModule,
          providers: [
            { provide: Configuration, useValue: configuration }
          ]
        };
    }
}
