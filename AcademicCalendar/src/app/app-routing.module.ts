import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticatedGuard } from './shared/guards';

const routes: Routes = [
  {
    path: '', loadChildren: () => import('./user/user.module').then(m => m.UserModule)
  },
  {
    path: 'account', loadChildren: () => import('./account/account.module').then(m => m.AccountModule)
  },
  {
    path: 'error', loadChildren: () => import('./error/error.module').then(m => m.ErrorModule), canActivate: [AuthenticatedGuard]
  },
  {
    path: '**', redirectTo: 'error'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
