import { Component, OnInit } from '@angular/core';
import { RolesService, StudiesService } from './academicCalendarClient/helpers';
import { ProgramStudiesService } from './academicCalendarClient/services/programStudies.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  title = 'AcademicCalendar';

  constructor(private rolesService: RolesService, private studiesService: StudiesService,
              private programStudiesService: ProgramStudiesService) {}

  async ngOnInit(): Promise<void> {
    this.rolesService.setRole();

    await this.programStudiesService.getAll()?.subscribe(
      response => {
        const programStudies = response.filter(r => r.UrlParameter === window.location.href.split('/').pop());
        if (programStudies.length > 0) {
          this.studiesService.changeCurrent(programStudies[0].UrlParameter);
          sessionStorage.setItem('studies', programStudies[0].UrlParameter);
        }
      }
    );
  }


}
