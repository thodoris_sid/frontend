import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-error-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class ErrorNotFoundComponent {

    constructor(private router: Router) {}

    routerLink(): void {
      if (sessionStorage.getItem('studies')) {
          this.router.navigateByUrl('/program/' + sessionStorage.getItem('studies'));
      } else {
          this.router.navigateByUrl('/');
      }
    }

}
