import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ErrorNotFoundComponent } from './components';

const routes: Routes = [
  {
    path: '', redirectTo: 'notFound' , pathMatch: 'full'
  },
  {
    path: 'notFound', component: ErrorNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErrorRoutingModule { }
