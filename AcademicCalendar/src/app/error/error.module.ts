import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';

import { SharedModule } from '../shared/shared.module';
import { ErrorNotFoundComponent } from './components';
import { ErrorRoutingModule } from './error-routing.module';

@NgModule({
  imports: [
    SharedModule, ErrorRoutingModule, MatCardModule, MatButtonModule
  ],
  declarations: [
    ErrorNotFoundComponent
  ]
})
export class ErrorModule { }
