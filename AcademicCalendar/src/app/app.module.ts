import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AcademicCalendarClientModule } from './academicCalendarClient/academicCalendarClient.module';
import { environment } from 'src/environments/environment';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule, AppRoutingModule, SharedModule,
    AcademicCalendarClientModule.forRoot({
      baseUrl: environment.baseUrl,
      oAuthStorageName: environment.oAuthStorageName,
      oAuthStorageSecured: environment.oAuthStorageSecured,
      oAuthStorageExpirationInDays: environment.oAuthStorageExpirationInDays
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
