import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';

import { ProfessorsAddComponent } from './add/add.component';
import { ProfessorsListComponent } from './list/list.component';
import { ProfessorsRoutingModule } from './professors-routing.module';
import { ProfessorsComponent } from './professors.component';

@NgModule({
    imports: [
        CommonModule, ReactiveFormsModule, ProfessorsRoutingModule, MatFormFieldModule, MatExpansionModule, MatProgressSpinnerModule,
        MatButtonModule, MatCardModule, MatDialogModule, MatIconModule, MatInputModule,
        MatTabsModule, MatListModule, MatSelectModule
    ],
    declarations: [
        ProfessorsComponent, ProfessorsListComponent, ProfessorsAddComponent
    ]
})
export class ProfessorsModule { }
