import { Component, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { RolesService } from 'src/app/academicCalendarClient/helpers';
import { Professor, ProfessorType } from 'src/app/academicCalendarClient/models/professor.model';
import { ProfessorService } from 'src/app/academicCalendarClient/services/professor.service';
import { SnackBarComponent } from 'src/app/shared/components';
import { ConfirmDeleteComponent } from 'src/app/shared/components/dialogs/confirm-delete/confirm-delete.component';

@Component({
    selector: 'app-user-professors-list',
    templateUrl: './list.component.html'
})
export class ProfessorsListComponent implements OnDestroy {

    panelOpenState = false;
    professors: Array<Professor> = [];
    isLoading = false;
    professorId: number | null;
    professorType: string;

    constructor(private rolesService: RolesService, private activatedRoute: ActivatedRoute, private professorService: ProfessorService,
                private dialog: MatDialog, private snackBar: MatSnackBar) {
        this.activatedRoute.queryParams.subscribe(params => {
            this.professorId = params.professorId;
        });

        this.activatedRoute.params.subscribe(params => {
            this.professorType = params.professorType;
            this.getProfessors();
        });
    }

    getProfessors(): void {
        this.isLoading = true;
        this.professorService.getAll()?.subscribe(
            response => {
                if (this.professorType === ProfessorType[ProfessorType.dep]) {
                    this.professors = response.filter(r => r.Type === ProfessorType.dep);
                } else if (this.professorType === ProfessorType[ProfessorType.edip]) {
                    this.professors = response.filter(r => r.Type === ProfessorType.edip);
                } else {
                    this.professors = response.filter(r => r.Type === ProfessorType.exwterikoi);
                }
                this.professors.sort((a, b) => a.LastName.localeCompare(b.LastName));
                this.isLoading = false;
            }
        );
    }

    delete(professorFirstName: string, professorLastName: string, professorId: number): void {

        const deleteCourseDialog = this.dialog.open(ConfirmDeleteComponent, {
            autoFocus: false,
            disableClose: false,
            data: {deletedItem: professorFirstName + ' ' + professorLastName, type: 'professor'}
        });
        deleteCourseDialog.afterClosed().subscribe(
            result => {
                if (result) {
                    this.isLoading = true;
                    this.professorService.delete(professorId).subscribe(
                        response => {
                            this.snackBar.openFromComponent(SnackBarComponent, {
                                duration: 4000,
                                data: 'Ο διδάσκων διαγράφηκε με επιτυχία',
                                panelClass: ['snackbar-success']
                            });
                            this.getProfessors();
                            this.isLoading = false;
                        }
                    );
                }
            }
        );

    }

    isSuperAdmin(): boolean {
        return this.rolesService.isSuperAdmin();
    }

    ngOnDestroy(): void {
        this.dialog.closeAll();
    }

}
