import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfessorsAddComponent } from './add/add.component';
import { ProfessorsListComponent } from './list/list.component';
import { ProfessorsComponent } from './professors.component';

const routes: Routes = [
    { path: '', component: ProfessorsComponent,
        children: [
            {
                path: ':professorType/:studies', component: ProfessorsListComponent
            }
        ]
    },
    {
        path: 'add', component: ProfessorsAddComponent
    },
    {
        path: 'edit', component: ProfessorsAddComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProfessorsRoutingModule { }
