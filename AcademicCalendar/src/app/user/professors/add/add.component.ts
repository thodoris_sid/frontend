import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NewAdmin } from 'src/app/academicCalendarClient/models/oAuth.model';
import { AddProfessor } from 'src/app/academicCalendarClient/models/professor.model';
import { AuthenticationService } from 'src/app/academicCalendarClient/services/authentication.service';
import { ProfessorService } from 'src/app/academicCalendarClient/services/professor.service';
import { noWhitespace } from 'src/app/shared/global';

@Component({
    selector: 'app-user-professors-add',
    templateUrl: './add.component.html'
})
export class ProfessorsAddComponent implements OnInit {

    isLoading = false;
    professorId = '';
    edit = false;

    addProfessorForm = this.formBuilder.group({
        firstName: ['', [Validators.required, noWhitespace]],
        lastName: ['', [Validators.required, noWhitespace]],
        description: [''],
        office: [''],
        phone: [''],
        email: ['', [Validators.required, Validators.email, noWhitespace]],
        website: [''],
        type: [0, [Validators.required]],
    });

    constructor(private router: Router, private activatedRoute: ActivatedRoute,
                private formBuilder: FormBuilder, private professorService: ProfessorService,
                private authenticationService: AuthenticationService) {}

    ngOnInit(): void {
        if (this.router.url.indexOf('edit') >= 0) {
            this.edit = true;
            this.activatedRoute.queryParams.subscribe(params => {
                this.professorId = params.professorId;
            });
            this.isLoading = true;
            this.professorService.get(+this.professorId)?.subscribe(
                response => {
                    this.firstNameFormField.setValue(response.FirstName);
                    this.lastNameFormField.setValue(response.LastName);
                    this.phoneFormField.setValue(response.Phone);
                    this.emailFormField.setValue(response.Email);
                    this.websiteFormField.setValue(response.Website);
                    this.officeFormField.setValue(response.Office);
                    this.descriptionFormField.setValue(response.Description);
                    this.typeFormField.setValue(response.Type);

                    this.isLoading = false;
                }, _ => {
                    this.routerLink('/professors/dep/');
                    this.isLoading = false;
                }
            );
        }
    }

    addProfessor(): void {

        const addProfessor: AddProfessor = {
            Firstname: this.firstNameFormField.value.trim(),
            Lastname: this.lastNameFormField.value.trim(),
            Description: this.descriptionFormField.value.trim(),
            Office: this.officeFormField.value.trim(),
            Phone: this.phoneFormField.value.trim(),
            Email: this.emailFormField.value.trim(),
            Type: this.typeFormField.value,
            Website: this.websiteFormField.value.trim(),
        };
        this.isLoading = true;
        if (this.edit) {
            this.professorService.update(addProfessor, +this.professorId).subscribe(
                response => {
                    this.isLoading = false;
                    this.routerLink('/professors/dep/');
                }, _ => {
                    this.isLoading = false;
                }
            );
        } else {

            const admin: NewAdmin = {
                Email: this.emailFormField.value.trim(),
                Password: 'Admin123!',
                ConfirmPassword: 'Admin123!'
            };

            this.professorService.add(addProfessor).subscribe(
                response => {
                    if (this.typeFormField.value === 0) {
                        this.authenticationService.register(admin).subscribe(
                            _ => {
                                this.isLoading = false;
                                this.routerLink('/professors/dep/');
                            }
                        );
                    } else {
                        this.isLoading = false;
                        this.routerLink('/professors/dep/');
                    }

                }, _ => {
                    this.isLoading = false;
                }
            );
        }

    }

    routerLink(url: string): void {
        if (sessionStorage.getItem('studies')) {
            this.router.navigateByUrl(url + sessionStorage.getItem('studies'));
        } else {
            this.router.navigateByUrl(url + 'PPS');
        }
    }


    get firstNameFormField(): FormControl { return this.addProfessorForm.get('firstName') as FormControl; }
    get lastNameFormField(): FormControl { return this.addProfessorForm.get('lastName') as FormControl; }
    get descriptionFormField(): FormControl { return this.addProfessorForm.get('description') as FormControl; }
    get officeFormField(): FormControl { return this.addProfessorForm.get('office') as FormControl; }
    get phoneFormField(): FormControl { return this.addProfessorForm.get('phone') as FormControl; }
    get emailFormField(): FormControl { return this.addProfessorForm.get('email') as FormControl; }
    get websiteFormField(): FormControl { return this.addProfessorForm.get('website') as FormControl; }
    get typeFormField(): FormControl { return this.addProfessorForm.get('type') as FormControl; }

}
