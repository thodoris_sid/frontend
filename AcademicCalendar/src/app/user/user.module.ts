import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';

import { BodyComponent, SidebarComponent } from '.';
import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
    imports: [
        CommonModule, ReactiveFormsModule , SharedModule, UserRoutingModule, MatToolbarModule, MatButtonModule, MatSidenavModule,
        MatListModule, MatIconModule, MatFormFieldModule, MatSelectModule
    ],
    declarations: [
        UserComponent, BodyComponent, SidebarComponent
    ]
})
export class UserModule { }
