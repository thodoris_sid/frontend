import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RolesService } from 'src/app/academicCalendarClient/helpers';
import { ProgramStudiesService } from 'src/app/academicCalendarClient/services/programStudies.service';
import { semestersTextPMS, semestersTextPPS } from 'src/app/globals';

@Component({
    selector: 'app-user-courses',
    templateUrl: './courses.component.html'
})
export class CoursesComponent implements OnInit {

    semesters: Array<string>;

    constructor(private rolesService: RolesService, private router: Router, private programStudiesService: ProgramStudiesService) {
        if (this.router.url === '/courses') {
            this.router.navigateByUrl('/courses/semester/1' + '/' + sessionStorage.getItem('studies'));
        }
    }

    ngOnInit(): void {
        this.programStudiesService.getAll()?.subscribe(
            _ => {
                this.semesters = sessionStorage.getItem('studies') && sessionStorage.getItem('studies') !== null
                // tslint:disable-next-line: no-non-null-assertion
                ? sessionStorage.getItem('studies')!.indexOf('PMS') >= 0 ? semestersTextPMS() : semestersTextPPS() : semestersTextPPS();
            }
        );
    }

    routerLink(index: number): string {
        if (sessionStorage.getItem('studies')) {
            return '/courses/semester/' + index + '/' + sessionStorage.getItem('studies');
        } else {
            return '/courses/semester/' + index + '/' + 'PPS';
        }
    }

    isSuperAdmin(): boolean {
        return this.rolesService.isSuperAdmin();
    }

}
