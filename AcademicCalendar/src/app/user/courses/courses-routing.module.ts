import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoursesAddComponent } from './add/add.component';
import { CoursesComponent } from './courses.component';
import { CoursesListComponent } from './list/list.component';
import { CoursesViewComponent } from './view/view.component';

const routes: Routes = [
    { path: '', component: CoursesComponent,
        children: [
            {
                path: 'semester/:semesterId/:studies', component: CoursesListComponent
            }
        ]
    },
    {
        path: 'semester/:semesterId/:courseId/:studies', component: CoursesViewComponent
    },
    {
        path: 'add', component: CoursesAddComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CoursesRoutingModule { }
