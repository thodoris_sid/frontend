import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSelectionList } from '@angular/material/list';
import { FiltersService } from 'src/app/academicCalendarClient/helpers';
import { Professor } from 'src/app/academicCalendarClient/models/professor.model';
import { ProfessorService } from 'src/app/academicCalendarClient/services/professor.service';

@Component({
    selector: 'app-user-courses-add-select-professor',
    templateUrl: './select-professor.component.html'
})
export class CoursesAddSelectProfessorComponent implements OnInit {

    @ViewChild('professorsList') professor: MatSelectionList;

    isLoading = false;
    professors: Array<Professor> = [];
    filteredProfessors: Array<Professor> = [];

    filterForm = this.formBuilder.group({
        professor: ['']
    });

    constructor(private dialogRef: MatDialogRef<CoursesAddSelectProfessorComponent>, @Inject(MAT_DIALOG_DATA) private data: any,
                private professorService: ProfessorService, private formBuilder: FormBuilder, private filtersService: FiltersService) {

        this.filterForm.valueChanges.subscribe(value => {
            this.professors = this.filteredProfessors.filter(
                professor => {
                    return (value.professor ? this.filtersService.removeAccents((professor.FirstName.toLowerCase().toString() + ' ' +
                    professor.LastName.toLowerCase().toString()))
                    .indexOf( this.filtersService.removeAccents(value.professor.toLowerCase())) !== -1 : true);
                }
            );
        });

    }


    ngOnInit(): void {

        this.isLoading = true;
        this.professorService.getAll()?.subscribe(
            response => {
                this.professors = response;
                this.professors.sort((a, b) => a.LastName.localeCompare(b.LastName));
                this.filteredProfessors = response;
                this.filteredProfessors.sort((a, b) => a.LastName.localeCompare(b.LastName));
                this.isLoading = false;
            }
        );

    }

    select(): void {
        this.dialogRef.close(
            {
                professorId: this.professor.selectedOptions.selected[0]?.value.ProfessorId,
                firstName: this.professor.selectedOptions.selected[0]?.value.FirstName,
                lastName: this.professor.selectedOptions.selected[0]?.value.LastName
            }
        );
    }

    close(): void {
        this.dialogRef.close();
    }

}

