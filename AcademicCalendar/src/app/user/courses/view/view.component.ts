import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { RolesService } from 'src/app/academicCalendarClient/helpers';
import { CourseWithProfessor } from 'src/app/academicCalendarClient/models/course.model';
import { CourseService } from 'src/app/academicCalendarClient/services/courses.service';
import { SnackBarComponent } from 'src/app/shared/components';
import { ConfirmDeleteComponent } from 'src/app/shared/components/dialogs/confirm-delete/confirm-delete.component';

@Component({
    selector: 'app-user-courses-view',
    templateUrl: './view.component.html'
})
export class CoursesViewComponent implements OnInit, OnDestroy {

    isLoading = false;
    course: CourseWithProfessor;
    professorType: number;
    semesterId: string | null;
    courseId: string | null;

    constructor(private rolesService: RolesService, private activatedRoute: ActivatedRoute,
                private router: Router, private courseService: CourseService,
                private dialog: MatDialog, private snackBar: MatSnackBar) {}

    ngOnInit(): void {
        this.semesterId = this.activatedRoute.snapshot.paramMap.get('semesterId');
        this.courseId = this.activatedRoute.snapshot.paramMap.get('courseId');
        this.isLoading = true;
        this.courseService.get(this.courseId).subscribe(
            response => {
                this.course = response;
                this.professorType = this.course.Professor.Type;
                this.isLoading = false;
            }, errorResponse => {
                this.router.navigateByUrl('/courses/semester/1');
                this.isLoading = false;
            }
        );
    }

    courseToProfessorRouter(): void {
        let router: string | null;
        if (this.professorType === 0) {
            router = '/professors/dep/' + (sessionStorage.getItem('studies') ?
            sessionStorage.getItem('studies') : 'PPS') + '?professorId=' + this.course.Professor.ProfessorId;
        } else if (this.professorType === 10) {
            router = '/professors/edip/' + (sessionStorage.getItem('studies') ?
            sessionStorage.getItem('studies') : 'PPS') + '?professorId=' + this.course.Professor.ProfessorId;
        } else {
            router = '/professors/exwterikoi/' + (sessionStorage.getItem('studies') ?
            sessionStorage.getItem('studies') : 'PPS') + '?professorId=' + this.course.Professor.ProfessorId;
        }

        // tslint:disable-next-line: no-non-null-assertion
        this.router.navigateByUrl(router!);

    }

    routerLink(): void {
        if (sessionStorage.getItem('studies')) {
            this.router.navigateByUrl('/courses/semester/' + this.semesterId + '/' + sessionStorage.getItem('studies'));
        } else {
            this.router.navigateByUrl('/courses/semester/' + this.semesterId + '/' + 'PPS');
        }
    }

    delete(): void {

        const deleteCourseDialog = this.dialog.open(ConfirmDeleteComponent, {
            autoFocus: false,
            disableClose: false,
            data: {deletedItem: this.course.Title, type: 'course'}
        });
        deleteCourseDialog.afterClosed().subscribe(
            result => {
                if (result) {
                    this.isLoading = true;
                    this.courseService.delete(this.courseId).subscribe(
                        response => {
                            this.snackBar.openFromComponent(SnackBarComponent, {
                                duration: 4000,
                                data: 'Το μάθημα διαφράφηκε με επιτυχία',
                                panelClass: ['snackbar-success']
                            });
                            if (sessionStorage.getItem('studies')) {
                                this.router.navigateByUrl('/courses/semester/' + this.semesterId + '/' + sessionStorage.getItem('studies'));
                            } else {
                                this.router.navigateByUrl('/courses/semester/' + this.semesterId + '/' + 'PPS');
                            }
                            this.isLoading = false;
                        }
                    );
                }
            }
        );

    }

    isSuperAdmin(): boolean {
        return this.rolesService.isSuperAdmin();
    }

    ngOnDestroy(): void {
        this.dialog.closeAll();
    }

}
