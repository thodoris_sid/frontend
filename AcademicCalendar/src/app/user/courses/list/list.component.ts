import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Course } from 'src/app/academicCalendarClient/models/course.model';
import { ProgramStudies } from 'src/app/academicCalendarClient/models/programStudies.model';
import { CourseService } from 'src/app/academicCalendarClient/services/courses.service';
import { ProgramStudiesService } from 'src/app/academicCalendarClient/services/programStudies.service';
import { numOfSemesters } from 'src/app/globals';

@Component({
    selector: 'app-user-courses-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class CoursesListComponent {

    emptyData = false;
    isLoading = false;
    semester: string;
    obligatoryCourses: Array<Course> = [];
    optionalCourses: Array<Course> = [];

    constructor(private activatedRoute: ActivatedRoute, private router: Router,
                private courseService: CourseService, private programStudiesService: ProgramStudiesService) {
            this.activatedRoute.params.subscribe(params => {
                this.semester = params.semesterId;
                this.getCoursesFromSemester();
            });
    }

    getCoursesFromSemester(): void {
        if (!this.semester || +this.semester > numOfSemesters()) {
            this.router.navigateByUrl('/courses/semester/1');
        }

        let studiesTypeEnum: Array<ProgramStudies> = [];

        this.programStudiesService.getAll()?.subscribe(
            response => {
                const studiesType = sessionStorage.getItem('studies') ? sessionStorage.getItem('studies') : 'PPS';
                const studies = response.filter(r => r.UrlParameter === studiesType);
                if (studies.length > 0) {
                    studiesTypeEnum = studies;
                } else {
                    studiesTypeEnum[0].StudiesId = 1;
                }

                this.isLoading = true;
                this.courseService.getAllFromSemester(this.semester, studiesTypeEnum[0].StudiesId)?.subscribe(
                    courses => {
                        this.obligatoryCourses = courses.filter(r => r.IsObligatory === true);
                        this.obligatoryCourses.sort((a, b) => a.Title.localeCompare(b.Title));
                        this.optionalCourses = courses.filter(r => r.IsObligatory === false);
                        this.optionalCourses.sort((a, b) => a.Title.localeCompare(b.Title));
                        this.isLoading = false;
                    }
                );

            }
        );

    }

    routerLink(course: any): void {
        if (sessionStorage.getItem('studies')) {
            this.router.navigateByUrl('/courses/semester/' + this.semester + '/' + course.CourseId
            + '/' + sessionStorage.getItem('studies'));
        } else {
            this.router.navigateByUrl('/courses/semester/' + this.semester + '/' + course.CourseId + '/' + 'PPS');
        }
    }

}
