import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AddCourse } from 'src/app/academicCalendarClient/models/course.model';
import { CourseService } from 'src/app/academicCalendarClient/services/courses.service';
import { noWhitespace } from 'src/app/shared/global';
import { CoursesAddSelectProfessorComponent } from '../select-professor/select-professor.component';

@Component({
    selector: 'app-user-courses-add',
    templateUrl: './add.component.html'
})
export class CoursesAddComponent {

    isLoading = false;
    professor = '';

    addCourseForm = this.formBuilder.group({
        courseCode: ['', [Validators.required, noWhitespace]],
        semester: [1, [Validators.required, Validators.min(1), Validators.max(8)]],
        title: ['', [Validators.required, noWhitespace]],
        description: ['', [Validators.required, noWhitespace]],
        professor: [{value: '', disabled: true}, [Validators.required]],
        studiesType: [0, [Validators.required]],
        obligatory: [true, [Validators.required]],
        equipment: this.formBuilder.array([this.initializeEquipment()])
    });

    constructor(private router: Router, private formBuilder: FormBuilder, private courseService: CourseService,
                private dialog: MatDialog) {}

    initializeEquipment(): FormGroup {
        return this.formBuilder.group({
            type: [{value: 'Χωρητικότητα', disabled: true}, [Validators.required]],
            typeCount: ['', [Validators.required, Validators.min(1)]]
        });
    }

    buildEquipment(): FormGroup {
        return this.formBuilder.group({
            type: ['', [Validators.required]],
            typeCount: [1, [Validators.required]]
        });
    }

    addEquipment(): void {
        this.equipment.push(this.buildEquipment());
    }

    removeEquipmentField(equipmentArrayIndex: number): void {
        (this.addCourseForm.get('equipment') as FormArray).removeAt(equipmentArrayIndex);
    }

    addProfessor(): void {
        const professor = this.dialog.open(CoursesAddSelectProfessorComponent, {
            autoFocus: false
        });

        professor.afterClosed().subscribe(result => {
            if (result) {
                this.professorFormField.setValue(result.professorId);
                this.professor = result.firstName + ' ' + result.lastName;
            }
        });

    }

    addCourse(): void {
        this.equipment.value[0].type = 'Χωρητικότητα';
        const addCourse: AddCourse = {
            CourseCode: this.courseCodeFormField.value.trim(),
            Semester: this.semesterFormField.value,
            Title: this.titleFormField.value.trim(),
            Description: this.descriptionFormField.value.trim(),
            ProfessorId: this.professorFormField.value,
            StudiesId: this.studiesTypeFormField.value,
            IsObligatory: this.obligatoryFormField.value,
            Equipment: this.equipment.value,
        };

        this.isLoading = true;
        this.courseService.add(addCourse).subscribe(
            response => {
                this.isLoading = false;
                this.routerLink('/courses/semester/1/');
            }, _ => {
                this.isLoading = false;
            }
        );
    }

    routerLink(url: string): void {
        if (sessionStorage.getItem('studies')) {
            this.router.navigateByUrl(url + sessionStorage.getItem('studies'));
        } else {
            this.router.navigateByUrl(url + 'PPS');
        }
    }


    get courseCodeFormField(): FormControl { return this.addCourseForm.get('courseCode') as FormControl; }
    get semesterFormField(): FormControl { return this.addCourseForm.get('semester') as FormControl; }
    get titleFormField(): FormControl { return this.addCourseForm.get('title') as FormControl; }
    get descriptionFormField(): FormControl { return this.addCourseForm.get('description') as FormControl; }
    get professorFormField(): FormControl { return this.addCourseForm.get('professor') as FormControl; }
    get studiesTypeFormField(): FormControl { return this.addCourseForm.get('studiesType') as FormControl; }
    get obligatoryFormField(): FormControl { return this.addCourseForm.get('obligatory') as FormControl; }
    get equipment(): FormArray { return this.addCourseForm.get('equipment') as FormArray; }
    get typeFormField(): FormControl { return this.addCourseForm.get('type') as FormControl; }
    get typeCountFormField(): FormControl { return this.addCourseForm.get('typeCount') as FormControl; }
}
