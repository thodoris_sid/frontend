import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatListModule } from '@angular/material/list';

import { CoursesRoutingModule } from './courses-routing.module';
import { CoursesComponent } from './courses.component';
import { CoursesListComponent } from './list/list.component';
import { CoursesViewComponent } from './view/view.component';
import { CoursesAddComponent } from './add/add.component';
import { CoursesAddSelectProfessorComponent } from './select-professor/select-professor.component';

@NgModule({
    imports: [
        CommonModule, ReactiveFormsModule, CoursesRoutingModule, MatButtonModule, MatCardModule, MatDialogModule,
        MatFormFieldModule, MatIconModule, MatInputModule, MatProgressSpinnerModule, MatSelectModule, MatSlideToggleModule,
        MatTabsModule, MatTableModule, MatListModule
    ],
    declarations: [
        CoursesComponent, CoursesListComponent, CoursesViewComponent, CoursesAddComponent, CoursesAddSelectProfessorComponent
    ]
})
export class CoursesModule { }
