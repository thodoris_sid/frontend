import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ProgramStudiesService } from 'src/app/academicCalendarClient/services/programStudies.service';
import { SnackBarComponent } from 'src/app/shared/components';
import { noWhitespace } from 'src/app/shared/global';
import { BodyComponent } from '../../components/body/body.component';

@Component({
    selector: 'app-user-studies-add',
    templateUrl: './add.component.html'
})
export class StudiesAddComponent {

    isLoading = false;
    baseUrl = '';

    addProgramStudiesForm = this.formBuilder.group({
        name: ['', [Validators.required, noWhitespace]],
        urlParameter: ['', [Validators.required, noWhitespace]]
    });

    constructor(private router: Router, private formBuilder: FormBuilder, private programStudiesService: ProgramStudiesService,
                private snackBar: MatSnackBar, private bodyComponent: BodyComponent) {
        this.baseUrl = window.location.host;
    }

    add(): void {
        this.isLoading = true;
        this.programStudiesService.add(this.nameFormField.value, this.urlParameterFormField.value).subscribe(
            _ => {
                this.bodyComponent.getProgramStudies();
                this.snackBar.openFromComponent(SnackBarComponent, {
                    duration: 4000,
                    data: 'Το πρόγραμμα σπουδών δημιουργήθηκε με επιτυχία',
                    panelClass: ['snackbar-success']
                });
                this.router.navigateByUrl('/studies');
                this.isLoading = false;
            }
        );
    }

    get nameFormField(): FormControl { return this.addProgramStudiesForm.get('name') as FormControl; }
    get urlParameterFormField(): FormControl { return this.addProgramStudiesForm.get('urlParameter') as FormControl; }

}
