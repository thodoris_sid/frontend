import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { StudiesAddComponent } from './add/add.component';
import { StudiesRoutingModule } from './studies-routing.module';
import { StudiesComponent } from './studies.component';

@NgModule({
    imports: [
        CommonModule, ReactiveFormsModule, StudiesRoutingModule, MatListModule, MatProgressSpinnerModule,
        MatButtonModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatListModule
    ],
    declarations: [
        StudiesComponent, StudiesAddComponent
    ]
})
export class StudiesModule { }
