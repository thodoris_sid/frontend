import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudiesAddComponent } from './add/add.component';
import { StudiesComponent } from './studies.component';

const routes: Routes = [
    {
        path: '', component: StudiesComponent
    },
    {
        path: 'add', component: StudiesAddComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class StudiesRoutingModule { }
