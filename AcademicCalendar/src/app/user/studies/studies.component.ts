import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ProgramStudies } from 'src/app/academicCalendarClient/models/programStudies.model';
import { ProgramStudiesService } from 'src/app/academicCalendarClient/services/programStudies.service';
import { SnackBarComponent } from 'src/app/shared/components';
import { ConfirmDeleteComponent } from 'src/app/shared/components/dialogs/confirm-delete/confirm-delete.component';
import { BodyComponent } from '../components/body/body.component';

@Component({
    selector: 'app-user-studies',
    templateUrl: './studies.component.html'
})
export class StudiesComponent implements OnInit, OnDestroy {

    isLoading = false;
    programStudies: Array<ProgramStudies> = [];

    constructor(private programStudiesService: ProgramStudiesService, private dialog: MatDialog, private snackBar: MatSnackBar,
                private bodyComponent: BodyComponent) {}

    ngOnInit(): void {
        this.getProgramStudies();
    }

    getProgramStudies(): void {
        this.isLoading = true;
        this.programStudiesService.getAll()?.subscribe(
            response => {
                this.programStudies = response;
                this.isLoading = false;
            }
        );
    }

    delete(studiesId: number, studiesName: string): void {

        const deleteStudiesDialog = this.dialog.open(ConfirmDeleteComponent, {
            autoFocus: false,
            disableClose: false,
            data: {deletedItem: studiesName, type: 'studies'}
        });
        deleteStudiesDialog.afterClosed().subscribe(
            result => {
                if (result) {
                    this.isLoading = true;
                    this.programStudiesService.delete(studiesId).subscribe(
                        response => {
                            this.snackBar.openFromComponent(SnackBarComponent, {
                                duration: 4000,
                                data: 'Το πρόγραμμα σπουδών διαγράφηκε με επιτυχία',
                                panelClass: ['snackbar-success']
                            });
                            this.getProgramStudies();
                            this.bodyComponent.getProgramStudies();
                            this.isLoading = false;
                        }
                    );
                }
            }
        );

    }


    ngOnDestroy(): void {
        this.dialog.closeAll();
    }

}

