import { CommonModule, registerLocaleData } from '@angular/common';
import localeEl from '@angular/common/locales/el';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { DragAndDropModule } from 'angular-draggable-droppable';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';

import { SharedModule } from 'src/app/shared/shared.module';
import { ProgramCoursesComponent } from './courses/list/list.component';
import { ProgramRoutingModule } from './program-routing.module';
import { ProgramComponent } from './program.component';
import { ProgramAddEventTimeComponent, ProgramEventInfoComponent, ProgramFooterComponent,
         ProgramHeaderComponent } from './parts';
import { ProgramInitialComponent } from './program-initial.component';

registerLocaleData(localeEl);

@NgModule({
    imports: [
        CommonModule, ReactiveFormsModule, ProgramRoutingModule, SharedModule, NgxMaterialTimepickerModule,
        CalendarModule.forRoot({
            provide: DateAdapter,
            useFactory: adapterFactory,
        }),
        DragAndDropModule, MatBadgeModule, MatButtonModule, MatDatepickerModule, MatDialogModule, MatExpansionModule,
        MatFormFieldModule, MatIconModule, MatInputModule, MatListModule, MatNativeDateModule, MatSelectModule,
        MatSlideToggleModule, MatSnackBarModule
    ],
    declarations: [
        ProgramComponent, ProgramCoursesComponent, ProgramHeaderComponent, ProgramFooterComponent,
        ProgramAddEventTimeComponent, ProgramEventInfoComponent, ProgramInitialComponent
    ]
})
export class ProgramModule { }
