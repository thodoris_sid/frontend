import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-user-program-initial',
    template: ''
})
export class ProgramInitialComponent {

    constructor(private router: Router) {
        if (sessionStorage.getItem('studies')) {
            this.router.navigateByUrl('/program/' + sessionStorage.getItem('studies'));
        } else {
            this.router.navigateByUrl('/program/PPS');
        }
    }

}
