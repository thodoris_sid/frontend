import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProgramInitialComponent } from './program-initial.component';
import { ProgramComponent } from './program.component';

const routes: Routes = [
    { path: '', component: ProgramInitialComponent },
    { path: ':studies', component: ProgramComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProgramRoutingModule { }
