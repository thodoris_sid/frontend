export const colors: any = {
    first: {
      primary: '#ad2121'
    },
    second: {
      primary: '#1e90ff'
    },
    third: {
      primary: '#006400'
    },
    forth: {
        primary: '#800080'
    },
    fifth: {
        primary: '#ffb84d'
    },
    sixth: {
        primary: '#000000'
    },
    seventh: {
        primary: '#ff99cc'
    },
    eighth: {
        primary: '#0000cc'
    }
};
