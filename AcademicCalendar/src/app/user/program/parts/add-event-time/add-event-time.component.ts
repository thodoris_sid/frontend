import { AfterViewInit, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {
    MAT_MOMENT_DATE_FORMATS,
    MomentDateAdapter,
    MAT_MOMENT_DATE_ADAPTER_OPTIONS,
  } from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatAccordion } from '@angular/material/expansion';
import * as moment from 'moment';
import { CacheHelperService } from 'src/app/academicCalendarClient/helpers';
import { Auditorium } from 'src/app/academicCalendarClient/models/auditorium.model';
import { Equipment } from 'src/app/academicCalendarClient/models/equipment.model';
import { Lecture } from 'src/app/academicCalendarClient/models/lecture.model';
import { AuditoriumService } from 'src/app/academicCalendarClient/services/auditorium.service';

@Component({
    selector: 'app-user-program-add-event-time',
    templateUrl: './add-event-time.component.html',
    providers: [
        {provide: MAT_DATE_LOCALE, useValue: 'el-EL'},
        {
          provide: DateAdapter,
          useClass: MomentDateAdapter,
          deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
        },
        {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
    ]
})
export class ProgramAddEventTimeComponent implements OnInit, AfterViewInit {

    @ViewChild(MatAccordion) accordion: MatAccordion;

    addLectureConflicts = false;
    lectureConflict = false;
    lectureConflicts: any[] = [];

    dateStart: Date;
    title = '';
    semester: number;
    equipment: Array<Equipment>;
    professorId: number;
    lectures: Array<Lecture>;

    panelOpenState = false;
    disableAnimation = true;
    isLoading = false;

    selectedAuditorium: Auditorium;
    auditoriums: Array<Auditorium> = [];

    invalidTime = false;

    isEquipmentCompatible = true;

    auditoriumsForm = this.formBuilder.group({
        auditorium: ['', Validators.required]
    });

    courseHoursForm = this.formBuilder.group({
        startHour: ['', [Validators.required]],
        endHour: ['', [Validators.required]]
    });

    recurringForm = this.formBuilder.group({
        recurring: [false, [Validators.required]],
        recurringEvery: [0],
        recurringUntil: ['']
    });

    constructor(private dialogRef: MatDialogRef<ProgramAddEventTimeComponent>, @Inject(MAT_DIALOG_DATA) data: any,
                private formBuilder: FormBuilder, private auditoriumService: AuditoriumService, private cacheHelper: CacheHelperService) {

        this.dateStart = data.newStart;
        this.title = data.title;
        this.semester = data.semester;
        this.equipment = data.equipment;
        this.professorId = data.professorId;
        this.lectures = data.lectures;

        this.courseHoursForm.valueChanges.subscribe(
            value => {
                if (moment(new Date().setHours(+value.startHour.split(':')[0], +value.startHour.split(':')[1], 0, 0)).toDate()
                >= moment(new Date().setHours(+value.endHour.split(':')[0], +value.endHour.split(':')[1], 0, 0)).toDate()) {
                    this.invalidTime = true;
                } else {
                    this.invalidTime = false;
                }
            }
        );
        this.auditoriumFormField.valueChanges.subscribe(
            value => {
                this.isEquipmentCompatible = true;
            }
        );
    }

    ngOnInit(): void {
        this.isLoading = true;
        this.cacheHelper.remove('getAllAuditoriums');
        this.auditoriumService.getAll()?.subscribe(
            response => {
                this.auditoriums = response;
                this.auditoriumFormField.setValue(response[0].AuditoriumId);
                this.isLoading = false;
            }
        );
    }

    ngAfterViewInit(): void {
        setTimeout(() => this.disableAnimation = false);
    }

    timeChangedStart(startTime: string): void {
        if (startTime === '01:00') {
            startTime = '08:00';
        }
        this.startHourFormField.setValue(startTime);
    }

    timeChangedEnd(endTime: string): void {
        this.endHourFormField.setValue(endTime);
    }

    add(): void {

        // CHECK IF I CAN ADD LECTURE - MERGE AUDITORIUM & PROFESSOR

        let startDate: Date = this.dateStart;
        this.recurringForm.value.recurringEvery = moment(startDate).day();
        const endDate = moment(new Date(this.recurringForm.value.recurringUntil)).toDate();


        if (this.recurringForm.value.recurring) {
            do {

                if (moment(startDate).day() === this.recurringForm.value.recurringEvery) {
                    this.checkLectureAvailability (startDate);
                }

                startDate = moment(startDate).add(1, 'days').toDate();

            } while (moment(startDate).format('DD-MM-YYYY') !== moment(endDate).add(1, 'days').format('DD-MM-YYYY'));

        } else {
            this.checkLectureAvailability (startDate);
        }

        if (this.lectureConflict) {
            this.addLectureConflicts = true;
       } else {
            this.dialogRef.close({hoursForm: this.courseHoursForm.value, recurringForm: this.recurringForm.value,
            auditorium: this.auditoriumFormField.value});
       }

    }

    checkLectureAvailability(startDate: Date): void {
        this.lectures.forEach(lecture => {
            if (this.professorId === lecture.professorId || this.auditoriumFormField.value === lecture.auditoriumId) {
                if
                (
                   moment.utc(startDate.setHours(+this.courseHoursForm.value.startHour.split(':')[0],
                   +this.courseHoursForm.value.startHour.split(':')[1], 0, 0)).local().toDate() >= lecture.start
                   && moment.utc(startDate.setHours(+this.courseHoursForm.value.startHour.split(':')[0],
                   +this.courseHoursForm.value.startHour.split(':')[1], 0, 0)).local().toDate() < lecture.end ||
                   moment.utc(startDate.setHours(+this.courseHoursForm.value.endHour.split(':')[0],
                   +this.courseHoursForm.value.endHour.split(':')[1], 0, 0)).local().toDate() > lecture.start
                   && moment.utc(startDate.setHours(+this.courseHoursForm.value.endHour.split(':')[0],
                   +this.courseHoursForm.value.endHour.split(':')[1], 0, 0)).local().toDate() <= lecture.end ||
                   lecture.start >= moment.utc(startDate.setHours(+this.courseHoursForm.value.startHour.split(':')[0],
                   +this.courseHoursForm.value.startHour.split(':')[1], 0, 0)).local().toDate()
                   && lecture.end <= moment.utc(startDate.setHours(+this.courseHoursForm.value.endHour.split(':')[0],
                   +this.courseHoursForm.value.endHour.split(':')[1], 0, 0)).local().toDate()
                ) {
                    this.lectureConflict = true;
                    if (this.professorId === lecture.professorId) {
                        this.lectureConflicts.push({conflictType: 'professor', conflictInfo: lecture.professorFirstName
                        + ' ' + lecture.professorLastName,
                        startHour: moment.utc(startDate.setHours(lecture.start.getHours())).local().toDate(),
                        endHour: moment.utc(startDate.setHours(lecture.end.getHours())).local().toDate()});
                    }

                    if (this.auditoriumFormField.value === lecture.auditoriumId){
                        this.lectureConflicts.push({conflictType: 'auditorium',
                        conflictInfo: this.auditoriums.filter(a => a.AuditoriumId = this.auditoriumFormField.value)[0].Name,
                        startHour: moment.utc(startDate.setHours(lecture.start.getHours())).local().toDate(),
                        endHour: moment.utc(startDate.setHours(lecture.end.getHours())).local().toDate()});
                    }
                }
            }
        });
    }

    checkEquipment(equipment: Equipment): boolean {
        this.selectedAuditorium = this.auditoriums.filter(a => a.AuditoriumId === this.auditoriumFormField.value)[0];
        if (this.selectedAuditorium && this.selectedAuditorium.Equipment.filter(e => e.Type === equipment.Type).length > 0) {
            const auditoriumEquipment = this.selectedAuditorium.Equipment.filter(e => e.Type === equipment.Type)[0];
            if (equipment.TypeCount <= auditoriumEquipment.TypeCount) {
                return true;
            } else {
                this.isEquipmentCompatible = false;
                return false;
            }
        }
        this.isEquipmentCompatible = false;
        return false;
    }

    close(): void {
        this.dialogRef.close();
    }

    get startHourFormField(): FormControl  { return this.courseHoursForm.get('startHour') as FormControl; }
    get endHourFormField(): FormControl  { return this.courseHoursForm.get('endHour') as FormControl; }
    get auditoriumFormField(): FormControl  { return this.auditoriumsForm.get('auditorium') as FormControl; }
    get recurringFormField(): FormControl  { return this.recurringForm.get('recurring') as FormControl; }
    get recurringEveryFormField(): FormControl  { return this.recurringForm.get('recurringEvery') as FormControl; }
    get recurringUntilFormField(): FormControl  { return this.recurringForm.get('recurringUntil') as FormControl; }

}
