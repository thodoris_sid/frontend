import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { CalendarView } from 'angular-calendar';
import { RolesService } from 'src/app/academicCalendarClient/helpers';
import { ProgramStudiesService } from 'src/app/academicCalendarClient/services/programStudies.service';
import { semestersTextPMS, semestersTextPPS } from 'src/app/globals';
import { ConfirmDeleteComponent } from 'src/app/shared/components/dialogs/confirm-delete/confirm-delete.component';

@Component({
    selector: 'app-user-program-header',
    templateUrl: './header.component.html'
})
export class ProgramHeaderComponent implements OnInit {
    @Input() view: CalendarView;

    @Input() viewDate: Date;

    @Input() locale = 'el';

    @Output() viewChange = new EventEmitter<CalendarView>();

    @Output() viewDateChange = new EventEmitter<Date>();

    @Output() delete = new EventEmitter<any>();

    @Output() download = new EventEmitter<any>();

    @Output() semester = new EventEmitter<any>();

    CalendarView = CalendarView;

    semesters: Array<string>;
    semesterForm = this.formBuilder.group({
        semester: ['']
    });

    isSearched = false;

    constructor(private rolesService: RolesService, private dialog: MatDialog, private formBuilder: FormBuilder,
                private programStudiesService: ProgramStudiesService) {
        this.semesterForm.valueChanges.subscribe(
            _ => {
                if (this.isSearched) {
                    this.semester.emit(this.semesterFormField.value);
                }
            }
        );

    }

    ngOnInit(): void {
        this.programStudiesService.getAll()?.subscribe(
            _ => {
                this.semesters = sessionStorage.getItem('studies') && sessionStorage.getItem('studies') !== null
                // tslint:disable-next-line: no-non-null-assertion
                ? sessionStorage.getItem('studies')!.indexOf('PMS') >= 0 ? semestersTextPMS() : semestersTextPPS() : semestersTextPPS();
                this.semesters.unshift('Όλα');
                this.semesterFormField.setValue(0);
                this.isSearched = true;
            }
        );
    }

    deleteLectures(): void {
        const deleteLecturesDialog = this.dialog.open(ConfirmDeleteComponent, {
            autoFocus: false,
            disableClose: false,
            data: {type: 'lectures'}
        });
        deleteLecturesDialog.afterClosed().subscribe(
            result => {
                if (result) {
                    this.delete.emit('delete');
                }
            }
        );
    }

    downloadPDF(): void {
        this.download.emit('download');
    }

    isAdmin(): boolean {
        return this.rolesService.isAdmin();
    }


    get semesterFormField(): FormControl  { return this.semesterForm.get('semester') as FormControl; }

}
