import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RolesService } from 'src/app/academicCalendarClient/helpers';
import { Auditorium } from 'src/app/academicCalendarClient/models/auditorium.model';
import { AuditoriumService } from 'src/app/academicCalendarClient/services/auditorium.service';
import { MyEvent } from '../../program.component';

@Component({
    selector: 'app-user-program-event-info',
    templateUrl: './event-info.component.html'
})
export class ProgramEventInfoComponent implements OnInit {

    event: MyEvent;
    startTime: Date;
    endTime: Date;
    auditorium = '';
    lectureId: number | undefined;
    courseId: number | undefined;
    auditoriums: Array<Auditorium> = [];
    isLoading = false;

    constructor(private dialogRef: MatDialogRef<ProgramEventInfoComponent>, @Inject(MAT_DIALOG_DATA) private data: any,
                private rolesService: RolesService, private auditoriumService: AuditoriumService) {}

    ngOnInit(): void {
        this.event = this.data.event;
        this.lectureId = this.event.lectureId;
        this.courseId = this.event.courseId;
        this.isLoading = true;
        this.auditoriumService.getAll()?.subscribe(
            response => {
                this.auditoriums = response;
                this.auditorium = this.auditoriums.filter(a => a.AuditoriumId = this.event.auditoriumId)[0].Name;
                this.isLoading = false;
            }
        );
    }

    close(): void {
        this.dialogRef.close();
    }

    delete(): void {
        this.dialogRef.close({action: 'delete', courseId: this.courseId});
    }

    isAdmin(): boolean {
        return this.rolesService.isAdmin();
    }

}
