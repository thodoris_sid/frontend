import { Component, ChangeDetectionStrategy, OnInit, OnDestroy } from '@angular/core';
import { forkJoin, Observable, Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

import {
  CalendarDateFormatter,
  CalendarEvent,
  CalendarEventTimesChangedEvent,
  CalendarView,
  DAYS_OF_WEEK,
} from 'angular-calendar';
import { isSameDay, isSameMonth } from 'date-fns';

import { CourseService } from 'src/app/academicCalendarClient/services/courses.service';
import { CustomDateFormatter } from './parts/date-formatter/date-formatter.component';
import { ProgramAddEventTimeComponent, ProgramEventInfoComponent } from './parts';
import { CacheHelperService, RolesService } from 'src/app/academicCalendarClient/helpers';
import * as moment from 'moment';
import { LectureService } from 'src/app/academicCalendarClient/services/lecture.service';
import { colors } from './parts/colors/colors';
import { SnackBarComponent } from 'src/app/shared/components';
import { Equipment } from 'src/app/academicCalendarClient/models/equipment.model';
import '../../../../src/assets/fonts/calibri-normal.js';
import { AuditoriumService } from 'src/app/academicCalendarClient/services/auditorium.service';
import { Auditorium } from 'src/app/academicCalendarClient/models/auditorium.model';
import { semestersText } from 'src/app/globals';
import { ProgramStudies } from 'src/app/academicCalendarClient/models/programStudies.model';
import { ProgramStudiesService } from 'src/app/academicCalendarClient/services/programStudies.service';


declare const require: any;

export interface MyEvent extends Omit<CalendarEvent, 'start'> {
    start: Date;
    courseId?: number;
    studiesId?: number;
    lectureId?: number;
    semester?: number;
    recurring?: boolean;
    recurringEvery?: number;
    recurringUntil?: Date;
    auditoriumId?: number;
    professorId?: number;
    professorFirstName?: string;
    professorLastName?: string;
    equipment?: Array<Equipment>;
}

@Component({
    selector: 'app-user-program',
    templateUrl: './program.component.html',
    styles: [
      `
          .drag-active {
            position: relative;
            z-index: 1;
            pointer-events: none;
          }
          .drag-over {
            background-color: #eee;
          }
        `
    ],
    providers: [
      {
        provide: CalendarDateFormatter,
        useClass: CustomDateFormatter,
      },
    ]
})
export class ProgramComponent implements OnInit, OnDestroy {
    isLoadingCourses = false;
    isLoadingLectures = false;
    CalendarView = CalendarView;

    viewDate = new Date();
    view = CalendarView.Month;

    externalEvents: MyEvent[] = [];

    events: MyEvent[] = [];
    eventsAllStudiesType: MyEvent[] = [];

    activeDayIsOpen = false;

    refresh = new Subject<void>();

    excludeDays: number[] = [0, 6];

    weekStartsOn = DAYS_OF_WEEK.SUNDAY;

    locale = 'el';

    semesters = semestersText();
    step = -1;

    recurringLecturesObservable: Observable<any>[] = [];

    auditoriums: Array<Auditorium> = [];

    constructor(private courseService: CourseService, private dialog: MatDialog, private rolesService: RolesService,
                private lectureService: LectureService, private snackBar: MatSnackBar, private cacheHelper: CacheHelperService,
                private auditoriumService: AuditoriumService, private programStudiesService: ProgramStudiesService) { }

    ngOnInit(): void {
        this.getAuditoriums();
        this.updateExternalEvents();
        this.updateProgramEvents();
    }

    getAuditoriums(): void {
        this.auditoriumService.getAll()?.subscribe(
            response => {
                this.auditoriums = response;
            }
        );
    }

    updateExternalEvents(): void {
        const loadingCourses = setTimeout(() => { this.isLoadingCourses = true; }, 100 );
        this.externalEvents = [];
        const studiesType = sessionStorage.getItem('studies') ? sessionStorage.getItem('studies') : 'PPS';

        let studiesTypeEnum: Array<ProgramStudies> = [];

        this.programStudiesService.getAll()?.subscribe(
            response => {
                const studies = response.filter(r => r.UrlParameter === studiesType);
                if (studies.length > 0) {
                    studiesTypeEnum = studies;
                } else {
                    studiesTypeEnum[0].StudiesId = 1;
                }

                this.courseService.getAll(studiesTypeEnum[0].StudiesId)?.subscribe(
                    courses => {
                        courses.forEach(event => {
                            if (!event.IsInCalendar) {
                                const course: MyEvent = {
                                    courseId: event.CourseId,
                                    title: event.Title,
                                    start: new Date(),
                                    end: undefined,
                                    draggable: true,
                                    semester: event.Semester,
                                    recurring: undefined,
                                    recurringEvery: undefined,
                                    recurringUntil: undefined,
                                    professorId: event.Professor.ProfessorId,
                                    professorFirstName: event.Professor.FirstName,
                                    professorLastName: event.Professor.LastName,
                                    equipment: event.Equipment,
                                    color: this.setColourBasedOnSemester(event.Semester)
                                };
                                this.externalEvents.push(course);
                                this.refresh.next();
                            }
                        });
                        if (loadingCourses) {
                            clearTimeout(loadingCourses);
                        }
                        this.isLoadingCourses = false;
                    }
                );

            }
        );

    }

    updateProgramEvents(semester?: number): void {
        const loadingLectures = setTimeout(() => { this.isLoadingLectures = true; }, 100 );
        this.events = [];
        const studiesType = sessionStorage.getItem('studies') ? sessionStorage.getItem('studies') : 'PPS';
        let studiesTypeEnum: Array<ProgramStudies> = [];

        this.programStudiesService.getAll()?.subscribe(
            response => {
                const studies = response.filter(r => r.UrlParameter === studiesType);
                if (studies.length > 0) {
                    studiesTypeEnum = studies;
                } else {
                    studiesTypeEnum[0].StudiesId = 1;
                }

                this.lectureService.getLecturesFromAllProgramStudies(studiesTypeEnum[0].StudiesId, semester)?.subscribe(
                    lectures => {
                        lectures.forEach(lecture => {
                            const course: MyEvent = {
                                courseId: lecture.CourseId,
                                studiesId: lecture.StudiesId,
                                title: lecture.CourseName,
                                start: moment.utc(lecture.StartTime).local().toDate(),
                                end: moment.utc(lecture.EndTime).local().toDate(),
                                draggable: false,
                                semester: lecture.Semester,
                                lectureId: lecture.LectureId,
                                professorFirstName: lecture.ProfessorFirstName,
                                professorLastName: lecture.ProfessorLastName,
                                professorId: lecture.ProfessorId,
                                auditoriumId: lecture.AuditoriumId,
                                color: this.setColourBasedOnSemester(lecture.Semester)
                            };
                            this.events.push(course);
                            this.refresh.next();
                        });
                        this.eventsAllStudiesType = this.events;
                        this.events = this.events.filter(e => e.studiesId === studiesTypeEnum[0].StudiesId);
                        if (loadingLectures) {
                            clearTimeout(loadingLectures);
                        }
                        this.isLoadingLectures = false;
                    }
                );

            }
        );

    }

    eventDropped({ event, newStart, newEnd }: CalendarEventTimesChangedEvent | any): void {

        if (event.end === undefined) {

            const addBusinessSegmentDialog = this.dialog.open(ProgramAddEventTimeComponent, {
                autoFocus: false,
                disableClose: false,
                data: {newStart, title: event.title, semester: event.semester, equipment: event.equipment,
                professorId: event.professorId, lectures: this.eventsAllStudiesType}
            });
            addBusinessSegmentDialog.afterClosed().subscribe(
                result => {
                    if (result) {

                        event.start = moment(new Date(newStart).setHours(+result.hoursForm.startHour.split(':')[0],
                        +result.hoursForm.startHour.split(':')[1], 0, 0)).toDate();
                        event.end = moment(new Date(newStart).setHours(+result.hoursForm.endHour.split(':')[0],
                        +result.hoursForm.endHour.split(':')[1], 0, 0)).toDate();
                        event.recurringEvery = new Date(newStart).getDay();
                        this.addEventIntoCalendar(event, result.hoursForm, result.recurringForm, result.auditorium);
                        this.refresh.next();
                    } else {
                        return;
                    }
                }
            );
        } else {
            event.start = newStart;
            event.end = newEnd;
            this.activeDayIsOpen = false;
            this.refresh.next();
        }
    }

    addEventIntoCalendar(event: MyEvent, result: any, recurring: any, auditorium: any): void {

        event.recurringUntil = recurring.recurringUntil;
        event.recurring = recurring.recurring;

        const externalIndex = this.externalEvents.indexOf(event);

        if (externalIndex > -1) {
            this.externalEvents.splice(externalIndex, 1);
            this.externalEvents = [...this.externalEvents];
            this.events.push(event);
            // RECURRING LOGIC -- START

            if (event.recurring && event.recurringUntil && event.end) {

                let startDate = event.start;
                const endDate = moment(new Date(event.recurringUntil)).toDate();

                do {
                    const course: MyEvent = {
                        courseId: event.courseId,
                        title: event.title,
                        start: startDate,
                        end: event.end,
                        draggable: false,
                        semester: event.semester,
                        color: event.color
                    };

                    if (moment(startDate).day() === event.recurringEvery) {
                        course.start = moment(new Date(startDate).setHours(+result.startHour.split(':')[0],
                        +result.startHour.split(':')[1], 0, 0)).toDate();
                        course.end = moment(new Date(startDate).setHours(+result.endHour.split(':')[0],
                        +result.endHour.split(':')[1], 0, 0)).toDate();
                        if (event.start !== startDate) {
                            this.events.push(course);
                        }
                        const courseInfo: any = {
                            StartTime: moment.utc(course.start).local().toDate(),
                            EndTime: moment.utc(course.end).local().toDate(),
                            CourseId: course.courseId,
                            AuditoriumId: auditorium
                        };
                        this.recurringLecturesObservable.push(this.lectureService.add(courseInfo));
                    }
                    startDate = moment(course.start).add(1, 'days').toDate();

                } while (moment(startDate).format('DD-MM-YYYY') !== moment(endDate).add(1, 'days').format('DD-MM-YYYY'));
                this.isLoadingLectures = true;
                forkJoin(this.recurringLecturesObservable)
                .subscribe(_ => {
                    this.cacheHelper.remove('getAllLectures');
                    this.cacheHelper.remove('getLecturesFromAllProgramStudies');
                    this.recurringLecturesObservable = [];
                    this.updateProgramEvents();
                    this.snackBar.openFromComponent(SnackBarComponent, {
                        duration: 4000,
                        data: 'Οι διαλέξεις προστέθηκαν με επιτυχία',
                        panelClass: ['snackbar-success']
                    });
                });

            } else {
                const courseInfo: any = {
                    StartTime: moment.utc(event.start).local().toDate(),
                    EndTime: moment.utc(event.end).local().toDate(),
                    CourseId: event.courseId,
                    AuditoriumId: auditorium
                };
                this.isLoadingLectures = true;
                this.lectureService.add(courseInfo).subscribe(
                    _ => {
                        this.cacheHelper.remove('getAllLectures');
                        this.cacheHelper.remove('getLecturesFromAllProgramStudies');
                        this.updateProgramEvents();
                        this.snackBar.openFromComponent(SnackBarComponent, {
                            duration: 4000,
                            data: 'Η διάλεξη προστέθηκε με επιτυχία',
                            panelClass: ['snackbar-success']
                        });
                    }
                );
            }

            // RECURRING LOGIC -- END
        }

        this.viewDate = event.start;
        this.activeDayIsOpen = false;
        this.events = [...this.events];
        this.step = -1;
    }

    externalDrop(event: MyEvent): void {
        event.start = new Date();
        event.end = undefined;
        if (this.externalEvents.indexOf(event) === -1) {
            this.events = this.events.filter((iEvent) => iEvent !== event);
            this.externalEvents.push(event);
            this.externalEvents = [...this.externalEvents];
            this.activeDayIsOpen = false;
        }
    }

    dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
        if (isSameMonth(date, this.viewDate)) {
            if ((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true)
            || events.length === 0) {
            this.activeDayIsOpen = false;
            } else {
            this.activeDayIsOpen = true;
            }
            this.viewDate = date;
        }
    }

    handleEvent(action: string, event: Omit<CalendarEvent, 'start'>): void {
        const eventInfo = this.dialog.open(ProgramEventInfoComponent, {
            data: {
                event
            },
            autoFocus: false,
            disableClose: false
        });

        eventInfo.afterClosed().subscribe(
            result => {
                if (result && result.action === 'delete') {
                    this.cacheHelper.remove('getAllAuditoriums');
                    this.getAuditoriums();
                    this.isLoadingCourses = true;
                    this.lectureService.delete(result.courseId).subscribe(
                        _ => {
                            this.cacheHelper.remove('getAllLectures');
                            this.cacheHelper.remove('getLecturesFromAllProgramStudies');
                            this.activeDayIsOpen = false;
                            this.updateExternalEvents();
                            this.updateProgramEvents();
                            this.snackBar.openFromComponent(SnackBarComponent, {
                                duration: 4000,
                                data: 'Οι διαλέξεις διαγράφηκαν με επιτυχία',
                                panelClass: ['snackbar-success']
                            });
                        }
                    );
                } else {
                    this.cacheHelper.remove('getAllAuditoriums');
                    this.getAuditoriums();
                }
            }
        );

    }

    isAdmin(): boolean {
        return this.rolesService.isAdmin();
    }

    setStep(index: number): void {
        this.step = index;
    }

    setColourBasedOnSemester(semester: number): any {
        return colors[Object.keys(colors)[semester - 1]];
    }

    badgeIndicator(semester: number): number {
        return this.externalEvents.filter(event => event.semester === semester).length;
    }

    hasSemesterAssignedCourses(semester: number): boolean {
        return this.externalEvents.some(event => event.semester === semester);
    }

    downloadPDF(): void {

        const lectures = this.events;
        // tslint:disable-next-line: no-non-null-assertion
        lectures.sort((a, b) => a.semester! - b.semester! || new Date(a.start).getTime() - new Date(b.start).getTime());

        const jsPDF = require('jspdf');
        require('jspdf-autotable');
        const doc = new jsPDF.jsPDF();
        doc.setFont('calibri');

        if (lectures.length === 0) {
            doc.text('Δεν υπάρχουν διαθέσιμες διαλέξεις', 63, 20);
        } else {
            doc.autoTable({
                head: [['Μάθημα', 'Εξάμηνο' , 'Αμφιθέατρο' , 'Διδάσκων', 'Ημερομηνία', 'Ώρα']],
                body: [
                    ...lectures.map(el => [el.title, el.semester, this.auditoriums.filter(a => a.AuditoriumId = el.auditoriumId)[0].Name,
                        el.professorFirstName + ' ' + el.professorLastName, moment.utc(el.start).local().format('DD-MM-YYYY'),
                        moment.utc(el.start).local().format('HH:mm') + ' - ' +  moment.utc(el.end).local().format('HH:mm')])
                ],
                styles: {
                  font: 'calibri',
                  fontStyle: 'normal',
                },
                margin: { bottom: 60 }
            });
        }


        doc.save('table.pdf');
        this.cacheHelper.remove('getAllAuditoriums');
    }

    deleteLectures(): void {
        this.isLoadingLectures = true;
        this.lectureService.deleteAll().subscribe(
            _ => {
                this.cacheHelper.remove('getAllLectures');
                this.cacheHelper.remove('getLecturesFromAllProgramStudies');
                this.activeDayIsOpen = false;
                this.updateExternalEvents();
                this.updateProgramEvents();
                this.snackBar.openFromComponent(SnackBarComponent, {
                    duration: 4000,
                    data: 'Οι διαλέξεις διαγράφηκαν με επιτυχία',
                    panelClass: ['snackbar-success']
                });
            }
        );
    }

    lecturesBySemester(semester: number): void {
        this.updateProgramEvents(semester);
    }

    ngOnDestroy(): void {
        this.dialog.closeAll();
    }

}
