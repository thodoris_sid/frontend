import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';

import { AuditoriumsAddComponent } from './add/add.component';
import { AuditoriumsRoutingModule } from './auditoriums-routing.module';
import { AuditoriumsComponent } from './auditoriums.component';
import { AuditoriumsListComponent } from './list/list.component';

@NgModule({
    imports: [
        CommonModule, ReactiveFormsModule, AuditoriumsRoutingModule, MatExpansionModule, MatListModule,
        MatProgressSpinnerModule, MatButtonModule, MatCardModule, MatDialogModule, MatFormFieldModule, MatIconModule,
        MatInputModule, MatSelectModule, MatTabsModule, MatListModule
    ],
    declarations: [
        AuditoriumsComponent, AuditoriumsListComponent, AuditoriumsAddComponent
    ]
})
export class AuditoriumsModule { }
