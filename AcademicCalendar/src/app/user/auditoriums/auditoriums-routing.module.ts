import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuditoriumsAddComponent } from './add/add.component';
import { AuditoriumsComponent } from './auditoriums.component';
import { AuditoriumsListComponent } from './list/list.component';

const routes: Routes = [
    { path: '', component: AuditoriumsComponent,
        children: [
            {
                path: ':auditoriumType/:studies', component: AuditoriumsListComponent
            }
        ]
    },
    {
        path: 'add', component: AuditoriumsAddComponent
    },
    {
        path: 'edit', component: AuditoriumsAddComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuditoriumsRoutingModule { }
