import { Component, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { RolesService } from 'src/app/academicCalendarClient/helpers';
import { Auditorium, AuditoriumType } from 'src/app/academicCalendarClient/models/auditorium.model';
import { AuditoriumService } from 'src/app/academicCalendarClient/services/auditorium.service';
import { SnackBarComponent } from 'src/app/shared/components';
import { ConfirmDeleteComponent } from 'src/app/shared/components/dialogs/confirm-delete/confirm-delete.component';

@Component({
    selector: 'app-user-auditoriums-list',
    templateUrl: './list.component.html'
})
export class AuditoriumsListComponent implements OnDestroy {

    panelOpenState = false;

    auditoriums: Array<Auditorium> = [];
    isLoading = false;
    auditoriumType: string;

    constructor(private rolesService: RolesService, private activatedRoute: ActivatedRoute, private auditoriumService: AuditoriumService,
                private dialog: MatDialog, private snackBar: MatSnackBar) {
        this.activatedRoute.params.subscribe(params => {
            this.auditoriumType = params.auditoriumType;
            this.getAuditoriums();
        });
    }

    getAuditoriums(): void {
        this.isLoading = true;
        this.auditoriumService.getAll()?.subscribe(
            response => {
                if (this.auditoriumType === AuditoriumType[AuditoriumType.classrooms]) {
                    this.auditoriums = response.filter(r => r.Type === AuditoriumType.classrooms);
                } else {
                    this.auditoriums = response.filter(r => r.Type === AuditoriumType.laboratories);
                }
                this.isLoading = false;
            }
        );
    }

    delete(auditoriumName: string, auditoriumId: number | undefined): void {

        const deleteCourseDialog = this.dialog.open(ConfirmDeleteComponent, {
            autoFocus: false,
            disableClose: false,
            data: {deletedItem: auditoriumName, type: 'auditorium'}
        });
        deleteCourseDialog.afterClosed().subscribe(
            result => {
                if (result) {
                    this.isLoading = true;
                    this.auditoriumService.delete(auditoriumId).subscribe(
                        response => {
                            this.snackBar.openFromComponent(SnackBarComponent, {
                                duration: 4000,
                                data: 'Το αμφιθέατρο διαγράφηκε με επιτυχία',
                                panelClass: ['snackbar-success']
                            });
                            this.getAuditoriums();
                            this.isLoading = false;
                        }
                    );
                }
            }
        );

    }

    isSuperAdmin(): boolean {
        return this.rolesService.isSuperAdmin();
    }

    ngOnDestroy(): void {
        this.dialog.closeAll();
    }

}
