import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { RolesService } from 'src/app/academicCalendarClient/helpers';

@Component({
    selector: 'app-user-auditoriums',
    templateUrl: './auditoriums.component.html'
})
export class AuditoriumsComponent {

    constructor(private rolesService: RolesService, private router: Router) {
        if (this.router.url === '/auditoriums') {
            this.router.navigateByUrl('/auditoriums/classrooms' + '/' + sessionStorage.getItem('studies'));
        }
    }

    routerLink(url: string): string {
        if (sessionStorage.getItem('studies')) {
            return url + sessionStorage.getItem('studies');
        } else {
            return url + '/' + 'PPS';
        }
    }

    isSuperAdmin(): boolean {
        return this.rolesService.isSuperAdmin();
    }

}
