import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AddAuditorium } from 'src/app/academicCalendarClient/models/auditorium.model';
import { AuditoriumService } from 'src/app/academicCalendarClient/services/auditorium.service';

@Component({
    selector: 'app-user-auditoriums-add',
    templateUrl: './add.component.html'
})
export class AuditoriumsAddComponent implements OnInit {


    isLoading = false;
    auditoriumId = '';
    edit = false;

    addAuditoriumForm = this.formBuilder.group({});

    constructor(private router: Router, private activatedRoute: ActivatedRoute,
                private formBuilder: FormBuilder, private auditoriumService: AuditoriumService) {}

    ngOnInit(): void {

        this.addAuditoriumForm = this.formBuilder.group({
            name: ['', [Validators.required]],
            capacity: [''],
            auditoriumType: [0, [Validators.required]],
            equipment: this.formBuilder.array(this.router.url.indexOf('edit') >= 0 ? [] : [this.initializeEquipment()])
        });

        if (this.router.url.indexOf('edit') >= 0) {
            this.edit = true;
            this.activatedRoute.queryParams.subscribe(params => {
                this.auditoriumId = params.auditoriumId;
            });

            this.isLoading = true;
            this.auditoriumService.get(+this.auditoriumId)?.subscribe(
                response => {
                    this.nameFormField.setValue(response.Name);
                    this.capacityFormField.setValue(response.Capacity);
                    this.auditoriumTypeFormField.setValue(response.Type);

                    response.Equipment.forEach((equipment, i) => {
                        if (i !== 0) {
                            this.equipment.push(this.formBuilder.group({
                                type: [equipment.Type],
                                typeCount: [equipment.TypeCount, [Validators.required]]
                            }));
                        } else {
                            this.equipment.push(this.formBuilder.group({
                                type: [{value: 'Χωρητικότητα', disabled: true}, [Validators.required]],
                                typeCount: [response.Capacity, [Validators.required, Validators.min(1)]]
                            }));
                        }
                    });
                    this.isLoading = false;
                }, _ => {
                    this.routerLink('/auditoriums/classrooms/');
                    this.isLoading = false;
                }
            );
        }
    }

    initializeEquipment(): FormGroup {
        return this.formBuilder.group({
            type: [{value: 'Χωρητικότητα', disabled: true}, [Validators.required]],
            typeCount: ['', [Validators.required, Validators.min(1)]]
        });
    }

    buildEquipment(): FormGroup {
        return this.formBuilder.group({
            type: ['', [Validators.required]],
            typeCount: [1, [Validators.required]]
        });
    }

    addEquipment(): void {
        this.equipment.push(this.buildEquipment());
    }

    removeEquipmentField(equipmentArrayIndex: number): void {
        (this.addAuditoriumForm.get('equipment') as FormArray).removeAt(equipmentArrayIndex);
    }

    addAuditorium(): void {

        this.equipment.value[0].type = 'Χωρητικότητα';
        const addAuditorium: AddAuditorium = {
            Name: this.nameFormField.value.trim(),
            Capacity: this.equipment.value[0].typeCount,
            Type: this.auditoriumTypeFormField.value,
            Equipment: this.equipment.value
        };
        this.isLoading = true;

        if (this.edit) {
            this.auditoriumService.update(addAuditorium, +this.auditoriumId).subscribe(
                response => {
                    this.isLoading = false;
                    this.routerLink('/auditoriums/classrooms/');
                }, _ => {
                    this.isLoading = false;
                }
            );
        } else {
            this.auditoriumService.add(addAuditorium).subscribe(
                response => {
                    this.isLoading = false;
                    this.routerLink('/auditoriums/classrooms/');
                }, _ => {
                    this.isLoading = false;
                }
            );
        }


    }

    routerLink(url: string): void {
        if (sessionStorage.getItem('studies')) {
            this.router.navigateByUrl(url + sessionStorage.getItem('studies'));
        } else {
            this.router.navigateByUrl(url + 'PPS');
        }
    }


    get nameFormField(): FormControl { return this.addAuditoriumForm.get('name') as FormControl; }
    get capacityFormField(): FormControl { return this.addAuditoriumForm.get('capacity') as FormControl; }
    get auditoriumTypeFormField(): FormControl { return this.addAuditoriumForm.get('auditoriumType') as FormControl; }
    get equipment(): FormArray { return this.addAuditoriumForm.get('equipment') as FormArray; }
    get typeFormField(): FormControl { return this.addAuditoriumForm.get('type') as FormControl; }
    get typeCountFormField(): FormControl { return this.addAuditoriumForm.get('typeCount') as FormControl; }

}
