import { Component, HostListener, AfterViewInit, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { CacheHelperService, RolesService, StudiesService } from 'src/app/academicCalendarClient/helpers';
import { ProgramStudies } from 'src/app/academicCalendarClient/models/programStudies.model';
import { ProgramStudiesService } from 'src/app/academicCalendarClient/services/programStudies.service';

@Component({
    selector: 'app-user-body',
    templateUrl: './body.component.html',
    styleUrls: ['./body.component.scss']
})
export class BodyComponent implements OnInit, AfterViewInit {

    sidebarShowToggler = true;
    sidebarOpened = false;
    innerWidth = window.innerWidth;
    programStudies: Array<ProgramStudies> = [];
    isSearched = false;

    studiesForm = this.formBuilder.group({
        studies: ['']
    });

    constructor(private rolesService: RolesService, private formBuilder: FormBuilder, private studiesService: StudiesService,
                private router: Router, private cacheHelperService: CacheHelperService,
                private programStudiesService: ProgramStudiesService) {

        this.studiesFormField.valueChanges.subscribe(
            value => {
                if (value !== sessionStorage.getItem('studies')) {
                    sessionStorage.setItem('studies', value);
                    this.cacheHelperService.remove('getAllCourses');
                    this.cacheHelperService.remove('getAllLectures');
                    this.cacheHelperService.remove('getLecturesFromAllProgramStudies');
                    if (this.isSearched) {
                        this.router.navigateByUrl('/program');
                    }
                }
            }
        );
    }

    @HostListener('window:resize', ['$event'])
    onResize(): void {
        this.innerWidth = window.innerWidth;
    }

    ngOnInit(): void {
        this.getProgramStudies();
    }

    public getProgramStudies(): void {

        this.programStudiesService.getAll()?.subscribe(
            response => {
                this.programStudies = response;
                this.studiesService.currentStudies.subscribe(value => {
                    this.studiesFormField.setValue(value);
                });
                this.isSearched = true;
            }
        );
    }

    ngAfterViewInit(): void {
        setTimeout(() => { this.sidebarOpened = true; }, 500);
    }


    isAdmin(): boolean {
        return this.rolesService.isAdmin();
    }

    isSuperAdmin(): boolean {
        return this.rolesService.isSuperAdmin();
    }

    get studiesFormField(): FormControl  { return this.studiesForm.get('studies') as FormControl; }

}
