import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-user-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {

    constructor(private router: Router) {}

    isActive(url: string): boolean {
        return this.router.url.indexOf(url) >= 0;
    }

    routerLink(url: string): void {
        if (sessionStorage.getItem('studies')) {
            this.router.navigateByUrl(url + sessionStorage.getItem('studies'));
        } else {
            this.router.navigateByUrl(url + 'PPS');
        }
    }

}
