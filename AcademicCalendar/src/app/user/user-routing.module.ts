import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './user.component';

const routes: Routes = [
    { path: '', component: UserComponent, children: [
        { path: '', redirectTo: 'program', pathMatch: 'full'},
        { path: 'program', loadChildren: () => import('./program/program.module').then(m => m.ProgramModule) },
        { path: 'courses', loadChildren: () => import('./courses/courses.module').then(m => m.CoursesModule) },
        { path: 'professors', loadChildren: () => import('./professors/professors.module').then(m => m.ProfessorsModule) },
        { path: 'auditoriums', loadChildren: () => import('./auditoriums/auditoriums.module').then(m => m.AuditoriumsModule) },
        { path: 'studies', loadChildren: () => import('./studies/studies.module').then(m => m.StudiesModule)}
    ] },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserRoutingModule { }
