export function numOfSemesters(): number {
    const noOfSemesters = sessionStorage.getItem('studies') && sessionStorage.getItem('studies') !== null
    // tslint:disable-next-line: no-non-null-assertion
    ? sessionStorage.getItem('studies')!.indexOf('PMS') >= 0 ? 2 : 8 : 8;

    return noOfSemesters;
}

export function semestersText(): Array<string> {
    const semestersPPS =
    ['1ο εξάμηνο', '2ο εξάμηνο', '3ο εξάμηνο', '4ο εξάμηνο', '5ο εξάμηνο', '6ο εξάμηνο', '7ο εξάμηνο', '8ο εξάμηνο'];
    const semestersPMS = ['1ο εξάμηνο', '2ο εξάμηνο'];

    const semesters = sessionStorage.getItem('studies') && sessionStorage.getItem('studies') !== null
    // tslint:disable-next-line: no-non-null-assertion
    ? sessionStorage.getItem('studies')!.indexOf('PMS') >= 0 ? semestersPMS : semestersPPS : semestersPPS;

    return semesters;

}

export function semestersTextPPS(): Array<string> {
    return ['1ο εξάμηνο', '2ο εξάμηνο', '3ο εξάμηνο', '4ο εξάμηνο', '5ο εξάμηνο', '6ο εξάμηνο', '7ο εξάμηνο', '8ο εξάμηνο'];
}

export function semestersTextPMS(): Array<string> {
    return ['1ο εξάμηνο', '2ο εξάμηνο'];
}

export function groupBy(list: any[], keyGetter: (arg0: any) => any): any {
    const map = new Map();
    list.forEach((item: any) => {
         const key = keyGetter(item);
         const collection = map.get(key);
         if (!collection) {
             map.set(key, [item]);
         } else {
             collection.push(item);
         }
    });
    return map;
}

