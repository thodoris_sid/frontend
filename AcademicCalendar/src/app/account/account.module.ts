import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatToolbarModule } from '@angular/material/toolbar';

import { SharedModule } from '../shared/shared.module';
import { AccountRoutingModule } from './account-routing.module';
import { AccountComponent } from './account.component';
import { AccountLoginComponent } from './components';

@NgModule({
    imports: [
        CommonModule, SharedModule, AccountRoutingModule, ReactiveFormsModule, MatCardModule, MatButtonModule,
        MatIconModule, MatInputModule, MatProgressSpinnerModule, MatToolbarModule
    ],
    declarations: [
        AccountComponent, AccountLoginComponent
    ]
})
export class AccountModule { }
