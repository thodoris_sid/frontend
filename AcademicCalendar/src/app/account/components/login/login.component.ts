import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RolesService } from 'src/app/academicCalendarClient/helpers';
import { AuthenticationService } from 'src/app/academicCalendarClient/services/authentication.service';

@Component({
    selector: 'app-account-login',
    templateUrl: './login.component.html'
})
export class AccountLoginComponent implements OnInit {

    isLoading = false;
    wrongCredentials = false;

    loginForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required]]
    });

    constructor(private formBuilder: FormBuilder, private router: Router, private authenticationService: AuthenticationService,
                private rolesService: RolesService) {}

    ngOnInit(): void {}

    login(): void {
        this.wrongCredentials = false;
        this.isLoading = true;
        this.authenticationService.tryLogin(this.emailFormField.value, this.passwordFormField.value, true).subscribe(
            response => {
                if (response) {
                    this.rolesService.setRole();
                    this.router.navigateByUrl('/');
                } else {
                    this.isLoading = false;
                    this.wrongCredentials = true;
                }
            }
        );
    }


    get emailFormField(): FormControl  { return this.loginForm.get('email') as FormControl; }
    get passwordFormField(): FormControl { return this.loginForm.get('password') as FormControl; }

}
