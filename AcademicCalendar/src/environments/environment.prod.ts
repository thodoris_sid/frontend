export const environment = {
  production: true,
  baseUrl: 'https://academic-calendar-thesis-api.azurewebsites.net',

  oAuthStorageName: 'academic-calendar-spa',
  oAuthStorageSecured: false,
  oAuthStorageExpirationInDays: 5
};
